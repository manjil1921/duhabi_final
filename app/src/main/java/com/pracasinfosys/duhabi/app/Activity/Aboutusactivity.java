package com.pracasinfosys.duhabi.app.Activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;


import com.pracasinfosys.duhabi.app.R;

public class Aboutusactivity extends AppCompatActivity {
Toolbar mtolbar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_aboutusactivity);
        mtolbar = (Toolbar)findViewById(R.id.aboutToolbar);
        setSupportActionBar(mtolbar);
        setTitle("About US");

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        mtolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

    }
}
