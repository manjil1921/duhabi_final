package com.pracasinfosys.duhabi.app.Activity;

import android.app.ProgressDialog;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.DownloadListener;
import com.androidnetworking.interfaces.DownloadProgressListener;
import com.pracasinfosys.duhabi.app.Adapter.UsersAdapter;
import com.pracasinfosys.duhabi.app.Interface_retrofit;
import com.pracasinfosys.duhabi.app.Model.Model_notice_array;
import com.pracasinfosys.duhabi.app.Model.Model_notice_main;
import com.pracasinfosys.duhabi.app.R;
import com.pracasinfosys.duhabi.app.Restapi.Apiclient;
import com.pracasinfosys.duhabi.app.Utils;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AllDownloads extends AppCompatActivity {
    Toolbar mtolbars;
    TextView mtext1,mtext2,mtext3,mtext4,mtext5,mtextgaupalika;
    String getactivity;
    Interface_retrofit minterface;
    ProgressDialog mdialogue;
    List<Model_notice_array> mlistss = new ArrayList<>();
    LinearLayout mlinearlayout,linear_gaupalika,linear1,linear2,linear3,linear4,linear5;
    ListView mlistviews;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_all_downloads);
        mtolbars = (Toolbar) findViewById(R.id.toolbar);
        mlistviews = (ListView) findViewById(R.id.mlistview);
        AndroidNetworking.initialize(this);
        mdialogue = new ProgressDialog(this);



        //names = getIntent().getExtras().getString("name_1");
        setSupportActionBar(mtolbars);
        //setTitle("वडाको विवरण ");
        //getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDefaultDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        mtolbars.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        getrestdata_wadasamachar();
        //getactivity = getIntent().getExtras().getString("activity");
//        if(getactivity.equals("samajik_surakshya")){
//           // getrestdata_samachar();
//
//        }else{
//            getrestdata_wadasamachar();
//        }

        mlistviews.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                // Toast.makeText(Odadetails_all.this, "Your click "+adapterView.getItemAtPosition(i), Toast.LENGTH_SHORT).show();
                Log.d("oda", "onItemClick: nnnnnnnnnitemsssssssssss" + i + "dfd" + adapterView.getItemIdAtPosition(i));
                int ids= (int) adapterView.getItemIdAtPosition(i);
                String fils = mlistss.get(ids).getFile();
                downloadingtask_files(fils,"Download_"+ids+".pdf");
               // Toast.makeText(AllDownloads.this, "the file is "+ fils, Toast.LENGTH_SHORT).show();

            }
        });

    }
    public void getrestdata_wadasamachar(){
        minterface= Apiclient.getApiclient().create(Interface_retrofit.class);
        Call<Model_notice_main> mnotice = minterface.mdownloads();
        mnotice.enqueue(new Callback<Model_notice_main>() {
            @Override
            public void onResponse(Call<Model_notice_main> call, Response<Model_notice_main> response) {
                mlistss = response.body().getResult();
                Log.d("faileds", "onFailure: failesd"+mlistss);
                //mrecycler.setAdapter(new Duhabi_newsadapter(getApplicationContext(),mlistss));
               // ArrayAdapter<List> madapter=new ArrayAdapter<List>(AllDownloads.this, android.R.layout.simple_list_item_1, mlistss);
                // mrecyclerview.setAdapter(new RecyclerView_odadetails(getApplicationContext(),mlistss,getactivity));
                // mlists.setAdapter(new UsersAdapter(getApplicationContext(),mlistss));
                 //ArrayAdapter<List<Model_notice_array>> madapter = new ArrayAdapter<List<Model_notice_array>>(this,android.R.layout.simple_list_item_1,mlistss.toArray());
                // mgetward.setAdapter(new Recyclerview_Suchanasamachar(mlistss,getApplicationContext()));
               mlistviews.setAdapter(new UsersAdapter(getApplicationContext(),mlistss));
            }

            @Override
            public void onFailure(Call<Model_notice_main> call, Throwable t) {
                Log.d("failed", "onFailure: failesd"+t.toString());
            }
        });

}
    public void progressdialogue(String messages){

        //mdialogue = new ProgressDialog(this);
        mdialogue.setMessage(messages);
        mdialogue.setIndeterminate(false);
        mdialogue.setCancelable(false);
        mdialogue.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        mdialogue.setProgress(10);
        mdialogue.show();
        //Handler mahand = new Handler();
        // mahand.postDelayed(new Runnable() {
        // @Override
        // public void run() {
        // mdialogue.dismiss();
        //  }
        // },4000);
    }
    public void downloadingtask_files(String fileurl,String flenamses){
        AndroidNetworking.download(fileurl, Environment.getExternalStorageDirectory()+"/"+ Utils.downloadDirectory,flenamses)
                .setTag("downloadTest")
                .setPriority(Priority.MEDIUM)
                .build()
                .setDownloadProgressListener(new DownloadProgressListener() {
                    @Override
                    public void onProgress(long bytesDownloaded, long totalBytes) {
                        // do anything with progress
                        //new Handler().postDelayed(new Runnable() {
                        //@Override
                        progressdialogue("Downloading...Please Wait..");
                        //public void run() {
                        //Toast.makeText(mcontext, "Downloading..Please Wait..", Toast.LENGTH_SHORT).show();
                        // }
                        //},5000);

                    }
                })
                .startDownload(new DownloadListener() {
                    @Override
                    public void onDownloadComplete() {
                        // do anything after completion
                        mdialogue.dismiss();
                        Toast.makeText(AllDownloads.this, "Download Finished", Toast.LENGTH_SHORT).show();
                        //PrintManager printManager = (PrintManager)mcontext.getSystemService(Context.PRINT_SERVICE);
                        //String jobName =mcontext.getString(R.string.app_name) +
                        //" Document";
                        // printManager.print(jobName, new MyPrintDocumentAdapter(Printing.this,getfilename),null);
                        // finish();
                    }
                    @Override
                    public void onError(ANError error) {
                        // handle error
                        mdialogue.dismiss();
                        Toast.makeText(AllDownloads.this, "Failed To Download..Please Try Again Later!!", Toast.LENGTH_SHORT).show();
                        Log.d("ll", "onError: errorddddddddddddddddd"+error.toString());
                    }
                });

    }
}

