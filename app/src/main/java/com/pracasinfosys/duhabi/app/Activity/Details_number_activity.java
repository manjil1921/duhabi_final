package com.pracasinfosys.duhabi.app.Activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.widget.TextView;

import com.pracasinfosys.duhabi.app.R;

public class Details_number_activity extends AppCompatActivity {

    Toolbar mtolbars;
    TextView name,post,phone,maddresss;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details_number_activity);
        mtolbars = (Toolbar)findViewById(R.id.ToolBars);
         name = (TextView)findViewById(R.id.mname);
        post =(TextView)findViewById(R.id.mtextview_post);
         phone = (TextView)findViewById(R.id.textview_phone);
        maddresss =(TextView)findViewById(R.id.textview_address);
        String getname = getIntent().getExtras().getString("name");
        String getpost = getIntent().getExtras().getString("post");
        String getphone = getIntent().getExtras().getString("phone");
        String getaddress = getIntent().getExtras().getString("address");
        name.setText(getname);
        post.setText(getpost);
        phone.setText(getphone);
        maddresss.setText(getaddress);

    }
}
