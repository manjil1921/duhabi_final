package com.pracasinfosys.duhabi.app.Activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Environment;
import android.print.PrintManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.DownloadListener;
import com.androidnetworking.interfaces.DownloadProgressListener;
import com.jacksonandroidnetworking.JacksonParserFactory;
import com.pracasinfosys.duhabi.app.Adapter.MyPrintDocumentAdapter;
import com.pracasinfosys.duhabi.app.R;
import com.pracasinfosys.duhabi.app.Utils;

public class Downloadable_neewactivity extends AppCompatActivity {
TextView toolbartextview,titletext,descriptiontext;
Button mviewbtn,mdownloadbtn,mprintbtn;
Toolbar mtolbars;
int id;
String gettitle,getdescriptio,getfileurl,gettoolbartext;
    ProgressDialog mdialogue;
    String namess,mdownloadfile,mprintfilename;
    int filetype;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_downloadables);
        mtolbars = (Toolbar)findViewById(R.id.toolbar);
        mdialogue = new ProgressDialog(this);

        descriptiontext = (TextView)findViewById(R.id.textview_detaislnoticedetails);
        titletext = (TextView)findViewById(R.id.textview_detailsnotices);
        mviewbtn =(Button)findViewById(R.id.btn_viewpdf);
        mdownloadbtn =(Button)findViewById(R.id.downloadpdfbtn);
        mprintbtn =(Button)findViewById(R.id.printbtn);
        toolbartextview =(TextView)findViewById(R.id.mtolbar_textview);
        setSupportActionBar(mtolbars);
        //gettitle = getIntent().getExtras().getString("titles");
        //Log.d("v", "onCreate: vvvvvvvvvvv"+gettitle);
        // setTitle(gettitle);
        // if(gettitle.equals("सुरक्षा भत्ता बारे जानकारी")){
        //titless.setVisibility(View.GONE);
        //  }else {
        // titless.setVisibility(View.VISIBLE);
        gettitle =getIntent().getExtras().getString("title");
        getdescriptio =getIntent().getExtras().getString("description");
        getfileurl=getIntent().getExtras().getString("fileurl");
        gettoolbartext =getIntent().getExtras().getString("tolbar_text");
        id= getIntent().getExtras().getInt("id");
        mdownloadfile = getIntent().getExtras().getString("downloadfile");
        mprintfilename = getIntent().getExtras().getString("printfilename");
        filetype = getIntent().getExtras().getInt("type");
        if(getfileurl.matches("nofile")){
            mviewbtn.setVisibility(View.GONE);
            mprintbtn.setVisibility(View.GONE);
            mdownloadbtn.setVisibility(View.GONE);

        }
            if(filetype == 1){
                mviewbtn.setVisibility(View.VISIBLE);
                mprintbtn.setVisibility(View.VISIBLE);
                mdownloadbtn.setVisibility(View.VISIBLE);

            }else if (filetype == 0){

                mviewbtn.setVisibility(View.GONE);
                mprintbtn.setVisibility(View.GONE);

            }

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        mtolbars.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        titletext.setText(gettitle);

//        }else{
//            mviewbtn.setVisibility(View.VISIBLE);
//            mprintbtn.setVisibility(View.VISIBLE);
//            mdownloadbtn.setVisibility(View.VISIBLE);
//        }
        descriptiontext.setText(getdescriptio);
        toolbartextview.setText(gettoolbartext);
        mviewbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                view_file();
            }
        });
        mdownloadbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //String downloadFileName = getfileurl.replace(Utils.mainUrl_socialssecurity, "");
                //progressdialogue("");
                //Toast.makeText(mcontext, "Downloading..Please Wait...", Toast.LENGTH_LONG).show();
                progressdialogue("Downloading...Please wait.");
               // if(filetype == 1){

                download_file(getfileurl,mdownloadfile);
//            }else{
//                    Intent mintenss = new Intent(Intent.ACTION_VIEW, Uri.parse(getfileurl));
//                    startActivity(mintenss);
//                    mdialogue.dismiss();
//
//                }
            }
        });
        mprintbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                print_file();
            }
        });

    }
    public void print_file(){
        namess = mprintfilename+id+".pdf";
        mdialogue = new ProgressDialog(this);
        progressdialogue("Loading...Please Wait..");
        AndroidNetworking.initialize(this);
        AndroidNetworking.setParserFactory(new JacksonParserFactory());
        AndroidNetworking.download(getfileurl, Environment.getExternalStorageDirectory()+"/"+ Utils.downloadDirectory,namess)
                .setTag("downloadTest")
                .setPriority(Priority.MEDIUM)
                .build()
                .setDownloadProgressListener(new DownloadProgressListener() {
                    @Override
                    public void onProgress(long bytesDownloaded, long totalBytes) {
                        // do anything with progress
                        //new Handler().postDelayed(new Runnable() {
                        //@Override
                        progressdialogue("Loading...Please Wait..");
                        //public void run() {
                        //Toast.makeText(Printing.this, "Printing..Please Wait..", Toast.LENGTH_SHORT).show();
                        //  }
                        //},5000);

                    }
                })
                .startDownload(new DownloadListener() {
                    @Override
                    public void onDownloadComplete() {
                        // do anything after completion
                        //
                        mdialogue.dismiss();
                        //finish();
                        Toast.makeText(Downloadable_neewactivity.this, "Finished", Toast.LENGTH_SHORT).show();
                        PrintManager printManager = (PrintManager)getSystemService(Context.PRINT_SERVICE);
                        String jobName =getString(R.string.app_name) +
                                " Document";
                        printManager.print(jobName, new MyPrintDocumentAdapter(Downloadable_neewactivity.this,namess),null);
                        //
                    }
                    @Override
                    public void onError(ANError error) {
                        // handle error
                        mdialogue.dismiss();
                        Toast.makeText(Downloadable_neewactivity.this, "Failed To Print..Please Try Again Later!!", Toast.LENGTH_SHORT).show();
                        Log.d("ll", "onError: errorddddddddddddddddd"+error.toString());
                    }
                });

    }
    public void view_file(){
        Intent mintenss = new Intent(Intent.ACTION_VIEW, Uri.parse(getfileurl));
        startActivity(mintenss);

    }
    public void download_file(String fileurl,String flenamses){
        AndroidNetworking.download(fileurl, Environment.getExternalStorageDirectory()+"/"+ Utils.downloadDirectory,flenamses)
                .setTag("downloadTest")
                .setPriority(Priority.MEDIUM)
                .build()
                .setDownloadProgressListener(new DownloadProgressListener() {
                    @Override
                    public void onProgress(long bytesDownloaded, long totalBytes) {
                        // do anything with progress
                        progressdialogue("Downloading...Please wait.");
                        //new Handler().postDelayed(new Runnable() {
                        //@Override
                        //public void run() {

                        //Toast.makeText(mcontext, "Downloading..Please Wait..", Toast.LENGTH_SHORT).show();
                        // }
                        //},5000);

                    }
                })
                .startDownload(new DownloadListener() {
                    @Override
                    public void onDownloadComplete() {
                        // do anything after completion
                        //
                        mdialogue.dismiss();
                        Toast.makeText(Downloadable_neewactivity.this, "Download Finished", Toast.LENGTH_SHORT).show();
                        //PrintManager printManager = (PrintManager)mcontext.getSystemService(Context.PRINT_SERVICE);
                        //String jobName =mcontext.getString(R.string.app_name) +
                        //" Document";
                        // printManager.print(jobName, new MyPrintDocumentAdapter(Printing.this,getfilename),null);
                        // finish();
                    }
                    @Override
                    public void onError(ANError error) {
                        // handle error

                        mdialogue.dismiss();
                        Toast.makeText(Downloadable_neewactivity.this, "Failed To Download..Please Try Again Later!!", Toast.LENGTH_SHORT).show();

                        Log.d("ll", "onError: errorddddddddddddddddd"+error.toString());
                    }
                });
    }
    public void progressdialogue(String messages){

        //mdialogue = new ProgressDialog(this);
        mdialogue.setMessage(messages);
        mdialogue.setIndeterminate(false);
        mdialogue.setCancelable(false);
        mdialogue.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        mdialogue.setProgress(10);
        mdialogue.show();
        //  Handler mahand = new Handler();
        //mahand.postDelayed(new Runnable() {
        // @Override
        //public void run() {
        //mdialogue.dismiss();
        //}
        //},4000);
    }
}
