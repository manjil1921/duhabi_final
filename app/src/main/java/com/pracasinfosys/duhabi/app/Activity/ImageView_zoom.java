package com.pracasinfosys.duhabi.app.Activity;


import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;

import com.pracasinfosys.duhabi.app.R;
import com.pracasinfosys.duhabi.app.TouchImageView;
import com.squareup.picasso.Picasso;

public class ImageView_zoom extends AppCompatActivity {
    TouchImageView mtouch;
    ImageButton mimageview;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image_view);
        mimageview = (ImageButton) findViewById(R.id.button_calcels);
        mtouch =(TouchImageView) findViewById(R.id.mtoucchimageview);
        String mimages = getIntent().getExtras().getString("image");
        Picasso.with(this).load(mimages).into(mtouch);
        mimageview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

    }
}
