package com.pracasinfosys.duhabi.app.Activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Environment;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.view.menu.MenuWrapperFactory;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.DownloadListener;
import com.androidnetworking.interfaces.DownloadProgressListener;
import com.pracasinfosys.duhabi.app.R;
import com.pracasinfosys.duhabi.app.Utils;
import com.squareup.picasso.Picasso;

public class Newscontent_details extends AppCompatActivity {
    TextView details,dates,titless;
    Toolbar mtoolbar;
    ImageView mimvae;
    String image,getfile;
    Button mbtn_pdffile,mbtn_download;
    ProgressDialog mdialogue;
    int typel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_newscontent_details);
        mtoolbar = (Toolbar)findViewById(R.id.toolbar);
        //String mtitle = getIntent().getExtras().getString("names_1");
        setSupportActionBar(mtoolbar);
       // setTitle(mtitle);
        mdialogue = new ProgressDialog(this);
         getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        mtoolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        details = (TextView)findViewById(R.id.textview_detaislnoticedetails);
        dates = (TextView)findViewById(R.id.textview_detailsnoticedates);
        titless = (TextView)findViewById(R.id.textview_detailsnotices);
        mimvae = (ImageView) findViewById(R.id.mtouch_imageview);
        mbtn_download = (Button) findViewById(R.id.button_downloadfile);
        mbtn_pdffile = (Button) findViewById(R.id.button_viewpdffile);

        //String image = getIntent().getExtras().getString("image");
        String detials = getIntent().getExtras().getString("detailcontent");
        String datess = getIntent().getExtras().getString("dates");
        String titles = getIntent().getExtras().getString("title");
        image = getIntent().getExtras().getString("iimage");
        getfile = getIntent().getExtras().getString("fil");
        typel = getIntent().getExtras().getInt("type");


        details.setText(detials);
        dates.setText(datess);
        titless.setText(titles);
        if(image.matches("Nofile")){
            mimvae.setVisibility(View.GONE);
           // return;
        }else{
            mimvae.setVisibility(View.VISIBLE);
            Picasso.with(this).load(image).into(mimvae);
            Log.d("ok", "onCreate: sdfsdfsdfsd"+image);
        }

        if(getfile.matches("No")){
            mbtn_pdffile.setVisibility(View.GONE);
            mbtn_download.setVisibility(View.GONE);
           // return;
        }else{
            if(typel == 0){
                mbtn_pdffile.setVisibility(View.GONE);
            }else{
                mbtn_pdffile.setVisibility(View.VISIBLE);
                mbtn_download.setVisibility(View.VISIBLE);
            }
//           mbtn_download.setVisibility(View.VISIBLE);
//           mbtn_pdffile.setVisibility(View.VISIBLE);
            mbtn_pdffile.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent minten = new Intent(Intent.ACTION_VIEW, Uri.parse(getfile));
                    startActivity(minten);
                    //  Toast.makeText(Newscontent_details.this, "sdfsdfsdf", Toast.LENGTH_SHORT).show();
                }
            });
            mbtn_download.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    download(getfile);
                    // Toast.makeText(Newscontent_details.this, "sdfsdfsdf", Toast.LENGTH_SHORT).show();
                }
            });

        }



        mimvae.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View view) {
               Intent mintent = new Intent(Newscontent_details.this, ImageView_zoom.class);
               mintent.putExtra("image",image);
               startActivity(mintent);

           }
       });
    }
    public void progressdialogue(String messages){

        //mdialogue = new ProgressDialog(this);
        mdialogue.setMessage(messages);
        mdialogue.setIndeterminate(false);
        mdialogue.setCancelable(false);
        mdialogue.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        mdialogue.setProgress(10);
        mdialogue.show();
//        Handler mahand = new Handler();
//        mahand.postDelayed(new Runnable() {
//            @Override
//            public void run() {
//                mdialogue.dismiss();
//            }
//        },3000);
    }
    public void download(String getfile){
        AndroidNetworking.initialize(this);
        AndroidNetworking.download(getfile, Environment.getExternalStorageDirectory()+"/"+ Utils.downloadDirectory,"NewsConten.pdf")
                .setTag("downloadTest")
                .setPriority(Priority.MEDIUM)
                .build()
                .setDownloadProgressListener(new DownloadProgressListener() {
                    @Override
                    public void onProgress(long bytesDownloaded, long totalBytes) {
                        // do anything with progress
                        //new Handler().postDelayed(new Runnable() {
                        //@Ov//erride
                        // public void run() {
                        progressdialogue("Downloading...Please Wait..");
                        // Toast.makeText(Sarbajanik_2_details.this, "Printing..Please Wait..", Toast.LENGTH_SHORT).show();
                        //  }
                        //   },5000);

                    }
                })
                .startDownload(new DownloadListener() {
                    @Override
                    public void onDownloadComplete() {
                        // do anything after completion
                        mdialogue.dismiss();
                        Toast.makeText(Newscontent_details.this, "Download Finished", Toast.LENGTH_SHORT).show();
                        //PrintManager printManager = (PrintManager)getSystemService(Context.PRINT_SERVICE);
                        //String jobName =getString(R.string.app_name) +
                        // " Document";
                        //printManager.print(jobName, new MyPrintDocumentAdapter(Sarbajanik_2_details.this,getfilename),null);
                        //finish();

                    }
                    @Override
                    public void onError(ANError error) {
                        // handle error
                        mdialogue.dismiss();
                        Toast.makeText(Newscontent_details.this, "Failed To Download..Please Try Again Later!!", Toast.LENGTH_SHORT).show();
                        Log.d("ll", "onError: errorddddddddddddddddd"+error.toString());
                    }
                });
         }

}
