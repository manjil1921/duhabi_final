package com.pracasinfosys.duhabi.app.Activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.pracasinfosys.duhabi.app.Adapter.RecyclerView_odadetails;
import com.pracasinfosys.duhabi.app.Interface_retrofit;
import com.pracasinfosys.duhabi.app.Model.Model_notice_array;
import com.pracasinfosys.duhabi.app.Model.Model_notice_main;
import com.pracasinfosys.duhabi.app.R;
import com.pracasinfosys.duhabi.app.Restapi.Apiclient;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Odadetails_all extends AppCompatActivity {
    Toolbar mtolbars;
    TextView mtext1, mtext2, mtext3, mtext4, mtext5, mtextgaupalika;
    String getactivity;
    LinearLayout mlinearlayout;
    RecyclerView mrecyclerview;
    RecyclerView.LayoutManager mlayoutmanager;
    List<Model_notice_array> mlistss = new ArrayList<>();
    Interface_retrofit minterface;

    //ListView mlists;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_odadetails_all);
        mtolbars = (Toolbar) findViewById(R.id.toolbar);
        //mrecycler=(RecyclerView)findViewById(R.id.mrecyclerview);
        //mswiperefresh =(SwipeRefreshLayout)findViewById(R.id.mswiprefresh);
        //mswiperefresh.setRefreshing(true);
//        mtext1 = (TextView) findViewById(R.id.textview_details_woda1);
//        mtext2 = (TextView) findViewById(R.id.textview_details_woda2);
//        mtext3 = (TextView) findViewById(R.id.textview_details_woda3);
//        mtext4 = (TextView) findViewById(R.id.textview_details_woda4);
//        mtext5 = (TextView) findViewById(R.id.textview_details_woda5);
//        mtextgaupalika = (TextView) findViewById(R.id.textview_details_gaupalika);
        mlinearlayout = (LinearLayout) findViewById(R.id.linear_woda);
        mrecyclerview = (RecyclerView)findViewById(R.id.mrecyc_layout);
        mrecyclerview.setLayoutManager(new LinearLayoutManager(this,LinearLayoutManager.VERTICAL,false));
        // mlists=(ListView) findViewById(R.id.mlist) ;
//        getrestdata_samachar();

        //names = getIntent().getExtras().getString("name_1");
        setSupportActionBar(mtolbars);
        //setTitle("वडाको विवरण ");
        //getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDefaultDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        mtolbars.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        getactivity = getIntent().getExtras().getString("activity");
        if(getactivity.equals("samparka") || getactivity.equals("padhadhikari")){
            getrestdata_samachar_padha();

        }else{
            getrestdata_sikshya();

        }
//        if (getactivity.equals("padhadhikari") || getactivity.equals("samparka")) {
//            //mtextgaupalika.setVisibility(View.VISIBLE);
//            mlinearlayout.setVisibility(View.VISIBLE);
////            mtextgaupalika.setOnClickListener(new View.OnClickListener() {
////                @Override
////                public void onClick(View view) {
////                    if (getactivity.equals("padhadhikari")) {
////                        Intent minten = new Intent(Odadetails_all.this, Padadhikairkobibaran_12.class);
////                        minten.putExtra("oda", "0");
////                        startActivity(minten);
////                    } else if (getactivity.equals("samparka")) {
////                        Intent min = new Intent(Odadetails_all.this, Samparka_13.class);
////                        min.putExtra("oda", "0");
////                        startActivity(min);
////                    }
////                }
////            });
//        }min
    }
    public void getrestdata_samachar_padha(){
        minterface= Apiclient.getApiclient().create(Interface_retrofit.class);
        Call<Model_notice_main> mnotice = minterface.mgetward();
        mnotice.enqueue(new Callback<Model_notice_main>() {
            @Override
            public void onResponse(Call<Model_notice_main> call, Response<Model_notice_main> response) {
                mlistss = response.body().getResult();
                Log.d("faileds", "onFailure: failesd"+mlistss);
                //mrecycler.setAdapter(new Duhabi_newsadapter(getApplicationContext(),mlistss));
//                ArrayAdapter<List> madapter=new ArrayAdapter<List>(this, android.R.layout.simple_list_item_1, mlistss);
                mrecyclerview.setAdapter(new RecyclerView_odadetails(getApplicationContext(),mlistss,getactivity));
                // mlists.setAdapter(new UsersAdapter(getApplicationContext(),mlistss));

//                mgetward.setAdapter(new Recyclerview_Suchanasamachar(mlistss,getApplicationContext()));


            }

            @Override
            public void onFailure(Call<Model_notice_main> call, Throwable t) {
                Log.d("failed", "onFailure: failesd"+t.toString());
            }
        });


    }
    public void getrestdata_sikshya(){
        minterface= Apiclient.getApiclient().create(Interface_retrofit.class);
        Call<Model_notice_main> mnotice = minterface.mgetward_samajik();
        mnotice.enqueue(new Callback<Model_notice_main>() {
            @Override
            public void onResponse(Call<Model_notice_main> call, Response<Model_notice_main> response) {
                mlistss = response.body().getResult();
                Log.d("faileds", "onFailure: failesd"+mlistss);
                //mrecycler.setAdapter(new Duhabi_newsadapter(getApplicationContext(),mlistss));
//                ArrayAdapter<List> madapter=new ArrayAdapter<List>(this, android.R.layout.simple_list_item_1, mlistss);
                mrecyclerview.setAdapter(new RecyclerView_odadetails(getApplicationContext(),mlistss,getactivity));
                // mlists.setAdapter(new UsersAdapter(getApplicationContext(),mlistss));

//                mgetward.setAdapter(new Recyclerview_Suchanasamachar(mlistss,getApplicationContext()));


            }

            @Override
            public void onFailure(Call<Model_notice_main> call, Throwable t) {
                Log.d("failed", "onFailure: failesd"+t.toString());
            }
        });

////        mtext1.setOnClickListener(new View.OnClickListener() {
////            @Override
////            public void onClick(View view) {
////                if (getactivity.equals("samajik")) {
////                    Intent minten = new Intent(Odadetails_all.this, Samajiksurakshya_5.class);
////                    minten.putExtra("oda", "1");
////                    startActivity(minten);
////                }else if(getactivity.equals("padhadhikari")){
////                    Intent minten = new Intent(Odadetails_all.this,Padadhikairkobibaran_12.class);
////                    minten.putExtra("oda","1");
////                    startActivity(minten);
////
////
////                }
////                else if(getactivity.equals("shikshya")){
////                    Intent minten = new Intent(Odadetails_all.this, Education_activity.class);
////                    minten.putExtra("oda", "1");
////                    startActivity(minten);
////
////
////                }
////                else if (getactivity.equals("health")){
////
////                    Intent minten = new Intent(Odadetails_all.this, Swasthay_activity.class);
////                    minten.putExtra("oda", "1");
////                    startActivity(minten);
////                }
////                else{
////                    Intent  min = new Intent(Odadetails_all.this,Samparka_13.class);
////                    min.putExtra("oda","1");
////                    startActivity(min);
////                }
////
////            }
////        });
////        mtext2.setOnClickListener(new View.OnClickListener() {
////            @Override
////            public void onClick(View view) {
////                if (getactivity.equals("samajik")) {
////                    Intent minten = new Intent(Odadetails_all.this, Samajiksurakshya_5.class);
////                    minten.putExtra("oda", "2");
////                    startActivity(minten);
////                }else if(getactivity.equals("padhadhikari")){
////                    Intent minten = new Intent(Odadetails_all.this,Padadhikairkobibaran_12.class);
////                    minten.putExtra("oda","2");
////                    startActivity(minten);
////
////                }
////                else if(getactivity.equals("shikshya")){
////                    Intent minten = new Intent(Odadetails_all.this, Education_activity.class);
////                    minten.putExtra("oda", "2");
////                    startActivity(minten);
////
////
////                }
////                else if (getactivity.equals("health")){
////
////                    Intent minten = new Intent(Odadetails_all.this, Swasthay_activity.class);
////                    minten.putExtra("oda", "2");
////                    startActivity(minten);
////                }
////                else{
////                    Intent  min = new Intent(Odadetails_all.this,Samparka_13.class);
////                    min.putExtra("oda","2");
////                    startActivity(min);
////                }
////            }
////        });
////        mtext3.setOnClickListener(new View.OnClickListener() {
////            @Override
////            public void onClick(View view) {
////                if (getactivity.equals("samajik")) {
////                    Intent minten = new Intent(Odadetails_all.this, Samajiksurakshya_5.class);
////                    minten.putExtra("oda", "3");
////                    startActivity(minten);
////                }else if(getactivity.equals("padhadhikari")){
////                    Intent minten = new Intent(Odadetails_all.this,Padadhikairkobibaran_12.class);
////                    minten.putExtra("oda","3");
////                    startActivity(minten);
////
////                }
////                else if(getactivity.equals("shikshya")){
////                    Intent minten = new Intent(Odadetails_all.this, Education_activity.class);
////                    minten.putExtra("oda", "3");
////                    startActivity(minten);
////
////
////                }
////                else if (getactivity.equals("health")){
////
////                    Intent minten = new Intent(Odadetails_all.this, Swasthay_activity.class);
////                    minten.putExtra("oda", "3");
////                    startActivity(minten);
////                }
////                else{
////                    Intent  min = new Intent(Odadetails_all.this,Samparka_13.class);
////                    min.putExtra("oda","3");
////                    startActivity(min);
////                }
////            }
////        });
////        mtext4.setOnClickListener(new View.OnClickListener() {
////            @Override
////            public void onClick(View view) {
////                if (getactivity.equals("samajik")) {
////                    Intent minten = new Intent(Odadetails_all.this, Samajiksurakshya_5.class);
////                    minten.putExtra("oda", "4");
////                    startActivity(minten);
////                }else if(getactivity.equals("padhadhikari")){
////                    Intent minten = new Intent(Odadetails_all.this,Padadhikairkobibaran_12.class);
////                    minten.putExtra("oda","4");
////                    startActivity(minten);
////
////                }else if(getactivity.equals("shikshya")){
////                    Intent minten = new Intent(Odadetails_all.this, Education_activity.class);
////                    minten.putExtra("oda", "4");
////                    startActivity(minten);
////
////
////                }
////                else if (getactivity.equals("health")){
////
////                    Intent minten = new Intent(Odadetails_all.this, Swasthay_activity.class);
////                    minten.putExtra("oda", "4");
////                    startActivity(minten);
////                }
////                else{
////                    Intent  min = new Intent(Odadetails_all.this,Samparka_13.class);
////                    min.putExtra("oda","4");
////                    startActivity(min);
////                }
////            }
////        });
////        mtext5.setOnClickListener(new View.OnClickListener() {
////            @Override
////            public void onClick(View view) {
////                if (getactivity.equals("samajik")) {
////                    Intent minten = new Intent(Odadetails_all.this, Samajiksurakshya_5.class);
////                    minten.putExtra("oda", "5");
////                    startActivity(minten);
////                }else if(getactivity.equals("padhadhikari")){
////                    Intent minten = new Intent(Odadetails_all.this,Padadhikairkobibaran_12.class);
////                    minten.putExtra("oda","5");
////                    startActivity(minten);
////
////                }
////                else if(getactivity.equals("shikshya")){
////                    Intent minten = new Intent(Odadetails_all.this, Education_activity.class);
////                    minten.putExtra("oda", "5");
////                    startActivity(minten);
////
////
////                }
////                else if (getactivity.equals("health")){
////
////                    Intent minten = new Intent(Odadetails_all.this, Swasthay_activity.class);
////                    minten.putExtra("oda", "5");
////                    startActivity(minten);
////                }
////                else{
////                    Intent  min = new Intent(Odadetails_all.this,Samparka_13.class);
////                    min.putExtra("oda","5");
////                    startActivity(min);
////                }
//            }
//        });
    }
}

