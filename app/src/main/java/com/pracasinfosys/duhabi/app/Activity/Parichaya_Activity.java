package com.pracasinfosys.duhabi.app.Activity;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.pracasinfosys.duhabi.app.Adapter.RecyclerView_Paaichaya;
import com.pracasinfosys.duhabi.app.Adapter.RecyclerView_updated;
import com.pracasinfosys.duhabi.app.Adapter.Recyclerview_socialsecurity;
import com.pracasinfosys.duhabi.app.Interface_retrofit;
import com.pracasinfosys.duhabi.app.Model.Model_notice_array;
import com.pracasinfosys.duhabi.app.Model.Model_notice_main;
import com.pracasinfosys.duhabi.app.R;
//import com.pracasinfosys.duhabi.app.Restapi.Apiclient;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;



/**
 * Created by Paru on 3/9/2018.
 */

public class Parichaya_Activity extends AppCompatActivity {
//    Interface_retrofit minterface;
//    RecyclerView mrecycler;
//    RecyclerView.LayoutManager mlayoutmanager;
//    Recyclerview_socialsecurity madapter;
    SwipeRefreshLayout mswiperefresh;
//    List<Model_notice_array> mlist_samajik = new ArrayList<>();
    Toolbar mtolbars;
    RequestQueue mrequest;
    Model_notice_array marray = new Model_notice_array();
    ImageView mimages_photo;
    TextView mtextview_getcontent;
    String getoda;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.parichaya_activity);
        mtolbars = (Toolbar) findViewById(R.id.toolbar);
        //mrecycler = (RecyclerView) findViewById(R.id.mrecyclerview);
//        mswiperefresh = (SwipeRefreshLayout) findViewById(R.id.mswiprefresh);
       // mswiperefresh.setRefreshing(true);
       // getoda = getIntent().getExtras().getString("oda");
        mrequest = Volley.newRequestQueue(this);
        mimages_photo =(ImageView) findViewById(R.id.imgparichaya);
        mtextview_getcontent =(TextView) findViewById(R.id.textview_parichaye);
        //names = getIntent().getExtras().getString("name_1");
        setSupportActionBar(mtolbars);
        //setTitle("सामाजिक सुरक्षा भत्ता");
        //getSupportActionBar().setDisplayHomeAsUpEnabled(true);
//        getSupportActionBar().setDefaultDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        mtolbars.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        //mswiperefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
           // @Override
           // public void onRefresh() {
               // mswiperefresh.setRefreshing(true);
                if (isconnectd()) {
                    getdata_voolley();
                    //Toast.makeText(Parichaya_Activity.this, "Refresh Successfull", Toast.LENGTH_SHORT).show();
                   // mswiperefresh.setRefreshing(false);
                } else {
                    //mlinear.setVisibility(View.VISIBLE);
                    Toast.makeText(Parichaya_Activity.this, "Failed to connect to Internet", Toast.LENGTH_SHORT).show();
                   // mswiperefresh.setRefreshing(false);
                }

           // }
       // });
//        mlayoutmanager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
//        mrecycler.setLayoutManager(mlayoutmanager);
        getdata_voolley();
    }

    public boolean isconnectd() {
        ConnectivityManager mconn = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo minfo = mconn.getActiveNetworkInfo();
        if (minfo != null && minfo.isConnected()) {
            return true;
        }
        return false;
    }

//    public void getreestapi_sarbajanikkharid() {
//        String ward_id = getoda;
//        minterface = Apiclient.getApiclient().create(Interface_retrofit.class);
//        Call<Model_notice_main> msarbajanik = minterface.mgetintro(ward_id);
//        msarbajanik.enqueue(new Callback<Model_notice_main>() {
//            @Override
//            public void onResponse(Call<Model_notice_main> call, Response<Model_notice_main> response) {
//                mlist_samajik = response.body().getResult();
//                Log.d("suc", "onResponse: kkkkkkkkkkk" + mlist_samajik);
//                mrecycler.setAdapter(new RecyclerView_Paaichaya(getApplicationContext(), mlist_samajik, "parichaya"));
//                mswiperefresh.setRefreshing(false);
//            }
//
//            @Override
//            public void onFailure(Call<Model_notice_main> call, Throwable t) {
//                Log.d("failed", "onResponse: kkkkkkkkkkk" + t.toString());
//            }
//        });
//
//    }

        public void getdata_voolley() {
            String urls = "http://pracasinfosys.com/duhabi/api/getIntro";
            JsonObjectRequest requestMessage = new JsonObjectRequest(Request.Method.GET, urls, new com.android.volley.Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    Log.v("About", response.toString());

                    //readWrite.writeFile("About.txt", response);
                    //Pref.sSavePreferences(getApplicationContext(),"About.txt", true);
                    parseAndDisplayMessage(response);

                    // final JSONObject result;
                    // try {
                    //  result = response.getJSONObject("result");
                    // emails = result.getString("email");
                    //Log.d("About", "onResponse: lllllllllllllllll"+emails);
                    // } catch (JSONException e) {
                    // e.printStackTrace();
                    //  }

                    // String body = result.getString("body");
                    // String subTitle = result.getString("sub_title");
                    // messageTitle.setText(Html.fromHtml(title));
                    //messageBody.setText(Html.fromHtml(body));
                    //messageSender.setText(Html.fromHtml(subTitle));


                }
            }, new com.android.volley.Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {

                }
            });

            mrequest.add(requestMessage);

        }

    public void parseAndDisplayMessage(JSONObject response) {


        try {
            JSONArray array = response.getJSONArray("result");

            // emails = result.getString("email");
            //Log.d("About", "onResponse: lllllllllllllllll"+emails);
            //String body = result.getString("body");
            // String subTitle = result.getString("sub_title");
            for (int i = 0; i < array.length(); i++) {

                JSONObject obj = array.getJSONObject(i);

                //Model_notice_array ema = new Model_notice_array(obj.getString("email"));
//                Model_notice_array marray = new Model_notice_array();
//                Log.d("About", "onResponse: lllllllllllllllll" + obj.getString("content"));
                //mlistemail.add(ema);
                //list.add("Select Email to Send");
                //list.add(obj.getString("email"));
                marray.setContent(obj.getString("content"));
                marray.setImage(obj.getString("image"));

            }
            Picasso.with(this).load(marray.getImage()).into(mimages_photo);
            mtextview_getcontent.setText(marray.getContent());


        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

}
