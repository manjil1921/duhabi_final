package com.pracasinfosys.duhabi.app.Activity;

import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.basgeekball.awesomevalidation.AwesomeValidation;
import com.pracasinfosys.duhabi.app.Interface_retrofit;
import com.pracasinfosys.duhabi.app.Model.Model_notice_array;
import com.pracasinfosys.duhabi.app.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

//import retrofit2.Response;

import static com.basgeekball.awesomevalidation.ValidationStyle.COLORATION;

public class Patrachargarnuhos_7 extends AppCompatActivity {
    Toolbar mtolbars;
    EditText msub,mname,messages_edit,maddress,mphone,mcitizenno,memail;
    String sub,name,msg,phone,address,email,citizenno;
    Button msend,mcance;
    Interface_retrofit mapai;
    List<Model_notice_array> mlistemail = new ArrayList<>();

    Spinner mspinner;
    String itemss;
   //ArrayList<String> list = new ArrayList<>();
    String emails;
    String email_name;
    RequestQueue mrequest;
    List<String> mlist = new ArrayList<>();

    List<Model_notice_array> marraylist = new ArrayList<>();
    AwesomeValidation mAwesomeValidation = new AwesomeValidation(COLORATION);
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_patrachargarnuhos_7);
        mtolbars = (Toolbar)findViewById(R.id.ToolBars);
        setSupportActionBar(mtolbars);
        //setTitle("पत्राचार गर्नुहोस");
        //getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDefaultDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        msend =(Button)findViewById(R.id.buttonsend);
        mcance =(Button)findViewById(R.id.buttoncancel);

        mAwesomeValidation.setColor(Color.RED);
        mspinner = (Spinner)findViewById(R.id.spinneritems);
        //mAwesomeValidation.addValidation(Patrachargarnuhos_7.this,R.id.spinneritems,"[a-zA-Z\\s]+",R.string.err_name);
        mrequest = Volley.newRequestQueue(this);
        getdata_voolley();
        mtolbars.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        msub =(EditText)findViewById(R.id.subjects);
        //mAwesomeValidation.addValidation(Patrachargarnuhos_7.this,R.id.subjects,"[a-zA-Z\\s]+",R.string.err_name);
        mname =(EditText)findViewById(R.id.username);
       // //mAwesomeValidation.addValidation(Patrachargarnuhos_7.this, R.id.username, "[a-zA-Z\\s]+", R.string.err_name);
        messages_edit =(EditText)findViewById(R.id.message_pat);
       // mAwesomeValidation.addValidation(Patrachargarnuhos_7.this, R.id.message_pat, "[a-zA-Z\\s]+", R.string.err_name);
        maddress=(EditText)findViewById(R.id.thegana);
       // mAwesomeValidation.addValidation(Patrachargarnuhos_7.this, R.id.thegana, "[a-zA-Z\\s]+", R.string.err_name);
        mphone =(EditText)findViewById(R.id.phone);
       // mAwesomeValidation.addValidation(Patrachargarnuhos_7.this, R.id.phone, "[0-9]+", R.string.err_name);
        mcitizenno =(EditText)findViewById(R.id.citizenship);
       // mAwesomeValidation.addValidation(Patrachargarnuhos_7.this, R.id.citizenship, "[0-9]+", R.string.err_name);
        memail = (EditText)findViewById(R.id.email);
       // mAwesomeValidation.addValidation(Patrachargarnuhos_7.this, R.id.email, android.util.Patterns.EMAIL_ADDRESS, R.string.err_email);
        mspinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
               itemss = adapterView.getItemAtPosition(i).toString();
              // for(int j=0;j<=i;){
                   emails = itemss.replace(adapterView.getItemAtPosition(i).toString(),marraylist.get(i).getEmail());
                   Log.d("e", "onItemSelected: mmmmmmmmmmm"+i);

              // }

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });


        msend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //sendEmail();
                //if (itemss.equals("")){
                   // Toast.makeText(Patrachargarnuhos_7.this, "Please Select Email to Send", Toast.LENGTH_SHORT).show();
                   // return;
              //  }
                Log.d("es", "onClick: [[[[[[[["+emails);
                name = mname.getText().toString();
                sub = msub.getText().toString();
                msg = messages_edit.getText().toString();
                phone = mphone.getText().toString();
                address = maddress.getText().toString();
                email =  memail.getText().toString().trim();
                citizenno = mcitizenno.getText().toString();
                //mAwesomeValidation.validate();

                String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";
                //String emails = email.matches(emailPattern);
                if(name.length()>0 ){
                    mname.setBackgroundResource(R.drawable.edittext_outerline);
                }else{
                    mname.setBackgroundResource(R.drawable.edittext_errorouterline);
                }
                if(sub.length()>0){
                    msub.setBackgroundResource(R.drawable.edittext_outerline);
                    //messages_edit.setText(null);


                }else{
                    msub.setBackgroundResource(R.drawable.edittext_errorouterline);
                }
                if(msg.length()>0){
                    messages_edit.setBackgroundResource(R.drawable.edittext_outerline);
                    //mphone.setText(null);
                   // mphone.setBackgroundResource(R.drawable.edittext_outerline);
                    //maddress.setText(null);
                   // maddress.setBackgroundResource(R.drawable.edittext_outerline);
                    //memail.setText(null);
                   // memail.setBackgroundResource(R.drawable.edittext_outerline);
                    //mcitizenno.setText(null);
                   // mcitizenno.setBackgroundResource(R.drawable.edittext_outerline);
                }else{
                    messages_edit.setBackgroundResource(R.drawable.edittext_errorouterline);
                }
                if(address.length()>0){
                    maddress.setBackgroundResource(R.drawable.edittext_outerline);
                }else{
                    maddress.setBackgroundResource(R.drawable.edittext_errorouterline);
                }
                if(email.matches(emailPattern)){
                    memail.setBackgroundResource(R.drawable.edittext_outerline);
                }else{
                   // Toast.makeText(Patrachargarnuhos_7.this, "Invalid Email", Toast.LENGTH_SHORT).show();
                    memail.setBackgroundResource(R.drawable.edittext_errorouterline);
                }
                if(citizenno.length()>0){
                    mcitizenno.setBackgroundResource(R.drawable.edittext_outerline);
                }else{
                    mcitizenno.setBackgroundResource(R.drawable.edittext_errorouterline);
                }
                if(phone.length()==10){
                    mphone.setBackgroundResource(R.drawable.edittext_outerline);
                }else{
                    mphone.setBackgroundResource(R.drawable.edittext_errorouterline);
                }
                if(name.length()>0 && sub.length()>0 && msg.length()>0 && address.length()>0 && email.matches(emailPattern) && citizenno.length()>0 && mphone.length() == 10){
                    post_compliant_restdata();
                    mname.setText(null);
                    msub.setText(null);
                    messages_edit.setText(null);
                    mphone.setText(null);
                    maddress.setText(null);
                    memail.setText(null);
                    mcitizenno.setText(null);
                    Toast.makeText(Patrachargarnuhos_7.this, "Sent Successfully", Toast.LENGTH_SHORT).show();
                    mname.setBackgroundResource(R.drawable.edittext_outerline);
                    //msub.setText(null);
                    msub.setBackgroundResource(R.drawable.edittext_outerline);
                    //messages_edit.setText(null);
                    messages_edit.setBackgroundResource(R.drawable.edittext_outerline);
                    //mphone.setText(null);
                    mphone.setBackgroundResource(R.drawable.edittext_outerline);
                    //maddress.setText(null);
                    maddress.setBackgroundResource(R.drawable.edittext_outerline);
                    //memail.setText(null);
                    memail.setBackgroundResource(R.drawable.edittext_outerline);
                    //mcitizenno.setText(null);
                    mcitizenno.setBackgroundResource(R.drawable.edittext_outerline);
                    mphone.setBackgroundResource(R.drawable.edittext_outerline);
                }else{
                    Toast.makeText(Patrachargarnuhos_7.this, "Please Fill All the field", Toast.LENGTH_SHORT).show();
                }


              /*  if(name.length()>0 && sub.length()>0 && msg.length()>0 && address.length()>0 && email.length()>0 && citizenno.length()>0){
                    //Toast.makeText(Patrachargarnuhos_7.this, "Please Fill All the filed", Toast.LENGTH_SHORT).show();
                    //return;
                    if(phone.length()==10) {
                        post_compliant_restdata();
                        mname.setText(null);
                        msub.setText(null);
                        messages_edit.setText(null);
                        mphone.setText(null);
                        maddress.setText(null);
                        memail.setText(null);
                        mcitizenno.setText(null);
                        Toast.makeText(Patrachargarnuhos_7.this, "Sent Successfully..", Toast.LENGTH_SHORT).show();
                        mname.setBackgroundResource(R.drawable.edittext_outerline);
                        //msub.setText(null);
                        msub.setBackgroundResource(R.drawable.edittext_outerline);
                        //messages_edit.setText(null);
                        messages_edit.setBackgroundResource(R.drawable.edittext_outerline);
                        //mphone.setText(null);
                        mphone.setBackgroundResource(R.drawable.edittext_outerline);
                        //maddress.setText(null);
                        maddress.setBackgroundResource(R.drawable.edittext_outerline);
                        //memail.setText(null);
                        memail.setBackgroundResource(R.drawable.edittext_outerline);
                        //mcitizenno.setText(null);
                        mcitizenno.setBackgroundResource(R.drawable.edittext_outerline);
                        //   }else if(phone.length()!=10 ){
                        //   Toast.makeText(Patrachargarnuhos_7.this, "Invalid Phone No.", Toast.LENGTH_SHORT).show();
                        // return;
                        // }else if (emails.equals("")){
                        // Toast.makeText(Patrachargarnuhos_7.this, "Please Fill All the filed", Toast.LENGTH_SHORT).show();
                        // return;
                    }else{
                        mphone.setBackgroundResource(R.drawable.edittext_errorouterline);
                        Toast.makeText(Patrachargarnuhos_7.this, "Invalid Phone No.", Toast.LENGTH_SHORT).show();
                    }
                }else{
                    mname.setBackgroundResource(R.drawable.edittext_errorouterline);
                    //msub.setText(null);
                    msub.setBackgroundResource(R.drawable.edittext_errorouterline);
                    //messages_edit.setText(null);
                    messages_edit.setBackgroundResource(R.drawable.edittext_errorouterline);
                    //mphone.setText(null);
                    mphone.setBackgroundResource(R.drawable.edittext_errorouterline);
                    //maddress.setText(null);
                    maddress.setBackgroundResource(R.drawable.edittext_errorouterline);
                    //memail.setText(null);
                    memail.setBackgroundResource(R.drawable.edittext_errorouterline);
                    //mcitizenno.setText(null);
                    mcitizenno.setBackgroundResource(R.drawable.edittext_errorouterline);
                    //Toast.makeText(Patrachargarnuhos_7.this, "Sent Successfully..", Toast.LENGTH_SHORT).show();
                    Toast.makeText(Patrachargarnuhos_7.this, "Please Fill All the filed", Toast.LENGTH_SHORT).show();
                }*/

            }
        });
        mcance.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mAwesomeValidation.clear();
                finish();
            }
        });


       // ArrayList<String> list = new ArrayList<>();

        //list.add(marary.getEmail());
        //list.add("kulchan.razu@gmail.com");
        //list.add("kulchan.srizan@gmail.com");


        //getdata();
        //Model_notice_array marray = new Model_notice_array();
        //marray.getEmail();
      // String emailsss = mlistemail.get(0).getEmail();
        //Log.d("em", "onCreate: emailssssssssssssssssss"+emailsss);
    }
    public void post_compliant_restdata(){



        String url = "http://pracasinfosys.com/duhabi/api/sendEmail";
        StringRequest postRequest = new StringRequest(Request.Method.POST, url, new com.android.volley.Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d("success", "onResponse: sssssssssssss"+response);
                msub.setText(null);
                messages_edit.setText(null);
                maddress.setText(null);
                mphone.setText(null);
                mcitizenno.setText(null);
                memail.setText(null);

            }
        }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("fai", "onResponse: sssssssssssss"+error.toString());
            }
        }){
            @Override
            protected Map<String, String> getParams()
            {
                Map<String, String>  params = new HashMap<String, String>();
                params.put("sent_to",emails);
                Log.d("final", "getParams: qqqqqqqqqqqqqqq"+itemss);
                params.put("name", name);
                params.put("address", address);
                params.put("phone", phone);
                params.put("email",email);
                params.put("citizen_no",citizenno);
                params.put("sub", sub);
                params.put("msg", msg);
                return params;
            }
        };
        mrequest.add(postRequest);
    }
   /* pu    }
blic void getdata(){
        mapai = Apiclient.getApiclient().create(Interface_retrofit.class);
        Call<Model_notice_main> getmailo= mapai.mgetEmail();
         getmailo.enqueue(new Callback<Model_notice_main>() {
           @Override
           public void onResponse(Call<Model_notice_main> call, Response<Model_notice_main> response) {
               mlistemail = response.body().getResult();
               //Log.d("em", "onCreate: emailssssssssssssssssss"+mlistemail.get(1).getEmail());
               //mlistemail.get(0).getEmail();
           }

           @Override
           public void onFailure(Call<Model_notice_main> call, Throwable t) {

           }
       });
}*/
   public void getdata_voolley(){
       String urls = "http://pracasinfosys.com/duhabi/api/getEmail";
        JsonObjectRequest requestMessage = new JsonObjectRequest(Request.Method.GET, urls, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.v("About", response.toString());

                //readWrite.writeFile("About.txt", response);
                //Pref.sSavePreferences(getApplicationContext(),"About.txt", true);
                parseAndDisplayMessage(response);

               // final JSONObject result;
               // try {
                  //  result = response.getJSONObject("result");
                   // emails = result.getString("email");
                    //Log.d("About", "onResponse: lllllllllllllllll"+emails);
               // } catch (JSONException e) {
                   // e.printStackTrace();
              //  }

                // String body = result.getString("body");
                   // String subTitle = result.getString("sub_title");
                   // messageTitle.setText(Html.fromHtml(title));
                    //messageBody.setText(Html.fromHtml(body));
                    //messageSender.setText(Html.fromHtml(subTitle));




            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });

        mrequest.add(requestMessage);

    }
    public void parseAndDisplayMessage(JSONObject response)
    {
        //spinner
        ArrayList<String> list = new ArrayList<>();

      // list.add("Select Email to Send");
        try {
            JSONArray array = response.getJSONArray("result");

           // emails = result.getString("email");
            //Log.d("About", "onResponse: lllllllllllllllll"+emails);
            //String body = result.getString("body");
           // String subTitle = result.getString("sub_title");

            for(int i = 0; i<array.length(); i++)
            {
                JSONObject obj = array.getJSONObject(i);
                Model_notice_array mararys = new Model_notice_array();
                //Model_notice_array ema = new Model_notice_array(obj.getString("email"));
                Log.d("About", "onResponse: lllllllllllllllll"+obj.getString("email"));
                //mlistemail.add(ema);
                //list.add("Select Email to Send");
                list.add(obj.getString("name"));
              //String listss = obj.getString("email");
              //mlist.add(obj.getString("name"));
                mararys.setEmail(obj.getString("email"));
               // Log.d("ttt", "parseAndDisplayMessage: eeeeeeeeeeeee"+listss);
                marraylist.add(mararys);
            }

            ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(this,android.R.layout.simple_spinner_item,list);
            arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            mspinner.setAdapter(arrayAdapter);
            //click listener on spinner items

        } catch (JSONException e)
        {
            e.printStackTrace();
        }
    }



}
