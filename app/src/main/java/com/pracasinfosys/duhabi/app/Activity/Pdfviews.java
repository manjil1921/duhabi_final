package com.pracasinfosys.duhabi.app.Activity;

import android.content.Context;
import android.os.Bundle;
import android.os.Environment;
import android.print.PrintManager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

import com.pracasinfosys.duhabi.app.Adapter.MyPrintDocumentAdapter;
import com.pracasinfosys.duhabi.app.R;
import com.github.barteksc.pdfviewer.PDFView;

import java.io.File;

/**
 * Created by user on 1/5/2018.
 */

public class Pdfviews extends AppCompatActivity {
   /* WebView wb;
    @Override
    public void onBackPressed(){
        if (wb.canGoBack()){
            wb.goBack();
        }else {
            super.onBackPressed();
        }
    }*/
   PDFView pdfView;
   Button mbtn,mprintbtn;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.pdffileloader);
       final String urls = getIntent().getExtras().getString("file");
        /*String url = "http://demo.acepirit.com/bsccsit/uploads/syllabus/0IWlu783.pdf";
        wb = (WebView)findViewById(R.id.webview);
        wb.getSettings().setJavaScriptEnabled(true);
        wb.setFocusable(true);
        wb.setFocusableInTouchMode(true);
        wb.getSettings().setRenderPriority(WebSettings.RenderPriority.HIGH);
        wb.getSettings().setCacheMode(WebSettings.LOAD_NO_CACHE);
        wb.getSettings().setDomStorageEnabled(true);
        wb.getSettings().setDatabaseEnabled(true);
        wb.getSettings().setAppCacheEnabled(true);
        wb.setScrollBarStyle(View.SCROLLBARS_INSIDE_OVERLAY);
        wb.loadUrl(url);
        wb.setWebViewClient(new WebViewClient());
*/// final File Output = new File(Environment.getExternalStorageDirectory() + "/"
             //   + Utils.downloadDirectory,urls);
        File Output = new File(Environment.getExternalStorageDirectory(),urls
           );
       mbtn = (Button) findViewById(R.id.mbtn_pdf);
       mprintbtn =(Button)findViewById(R.id.mbtn_print);
       mprintbtn.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View view) {
               PrintManager printManager = (PrintManager)getSystemService(Context.PRINT_SERVICE);
               String jobName = getString(R.string.app_name) +
                       " Document";
               printManager.print(jobName, new MyPrintDocumentAdapter(Pdfviews.this,urls),null);

           }
       });
       mbtn.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View view) {
               finish();
           }
       });

        pdfView = (PDFView) findViewById(R.id.pdfView);
       pdfView.fromFile(Output)
        //pdfView.fromSource(urls)
                .pages(0, 2, 1, 3, 3, 3) // all pages are displayed by default
                .enableSwipe(true) // allows to block changing pages using swipe
                .swipeHorizontal(false)
                .enableDoubletap(true)
                .defaultPage(0)
                // allows to draw something on the current page, usually visible in the middle of the screen
               // .onDraw(onDrawListener)
                // allows to draw something on all pages, separately for every page. Called only for visible pages
               // .onDrawAll(onDrawListener)
               // .onLoad(onLoadCompleteListener) // called after document is loaded and starts to be rendered
                //.onPageChange()
               /// .onPageScroll(onPageScrollListener)
                //.onError(onErrorListener)
               // /.onPageError(onPageErrorListener)
                //.onRender(onRenderListener) // called after document is rendered for the first time
                // called on single tap, return true if handled, false to toggle scroll handle visibility
                //.onTap(onTapListener)
                .enableAnnotationRendering(false) // render annotations (such as comments, colors or forms)
                .password(null)
               .scrollHandle(null)
               .enableAntialiasing(true) // improve rendering a little bit on low-res screens
                //spacing between pages in dp. To define spacing color, set view background
               .spacing(0)
                //.linkHandler(DefaultLinkHandler)
              //  .pageFitPolicy(FitPolicy.WIDTH)
                .load();

    }
}
