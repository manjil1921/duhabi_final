package com.pracasinfosys.duhabi.app.Activity;

import android.app.Activity;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Environment;
import android.os.Handler;
import android.print.PrintManager;
import android.util.Log;
import android.widget.Button;
import android.widget.Toast;

import com.pracasinfosys.duhabi.app.Adapter.MyPrintDocumentAdapter;
import com.pracasinfosys.duhabi.app.Checkforsdcard;
import com.pracasinfosys.duhabi.app.R;
import com.pracasinfosys.duhabi.app.Utils;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by user on 1/23/2018.
 */

public class Printingactivity extends Activity{
    private static final String TAG = "Download Task";
    private Context context;
    private Button buttonText;
    private String downloadUrl = "", downloadFileName = "",questons="";
    //private ProgressDialog mdialgoue = new ProgressDialog(context);
    String a="";
    public Printingactivity(Context context,String a) {
        this.context = context;
        this.a = a;
    }

    public Printingactivity(Context context, String downloadUrl, String questons) {
        this.context = context;

        this.downloadUrl = downloadUrl;
        this.questons=questons;
        if(questons.equals("publicpurchase")) {
            downloadFileName = downloadUrl.replace(Utils.mainUrl_publicpurchase, "");//Create file name by picking download file name from URL
            // Log.e(TAG, downloadFileName);
            // return;
        }else if(questons.equals("taxcost")) {
            downloadFileName = downloadUrl.replace(Utils.mainUrl_taxcost, "");//Create file name by picking download file name from URL
            // Log.e(TAG, downloadFileName);
        }else if(questons.equals("ApplicationFormat")){
            downloadFileName = downloadUrl.replace(Utils.mainUrl_applicationformat, "");//Create file name by picking download file name from URL
            //   Log.e(TAG, downloadFileName);
        }else if(questons.equals("socialsecurity")){
            downloadFileName = downloadUrl.replace(Utils.mainUrl_socialssecurity, "");//Create file name by picking download file name from URL
            //  Log.e(TAG, downloadFileName);
        }else if (questons.equals("vital_registration")){
            downloadFileName = downloadUrl.replace(Utils.mainUrl_vitalregistration, "");

        }else if (questons.equals("personalsecurity")){
            downloadFileName = downloadUrl.replace(Utils.mainUrl_personalsecurity, "");


        }else if (questons.equals("consumer")){
            downloadFileName = downloadUrl.replace(Utils.mainUrl_consumer, "");
        }else {
            return;
        }

        //downloadFileName = downloadUrl.replace(Utils.downloadPdfUrl, "");
        //Start Downloading Task
        new Printingactivity.DownloadingTask().execute();
    }
    /* public void downloadAndOpenPDF() {
         new Thread(new Runnable() {
             public void run() {
                 Uri path = Uri.fromFile(Downloadclass(this,downloadUrl));
                 try {
                     Intent intent = new Intent(Intent.ACTION_VIEW);
                     intent.setDataAndType(path, "application/pdf");
                     intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                     startActivity(intent);
                     finish();
                 } catch (ActivityNotFoundException e) {
                     Toast.makeText(Sarbajanik_2_details.this, "No Pdf Reader Application Installed", Toast.LENGTH_SHORT).show();
                 }
             }
         }).start();

     }*/
    /*public Downloadclass(Context context, String downloadUrl) {
        this.context = context;

        this.downloadUrl = downloadUrl;

        downloadFileName = downloadUrl.replace(Utils.mainUrl, "");//Create file name by picking download file name from URL
        Log.e(TAG, downloadFileName);

        //Start Downloading Task
        new DownloadingTask().execute();
    }*/
    private class DownloadingTask extends AsyncTask<Void, Void, Void> {

        File apkStorage = null;
        File outputFile = null;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            //buttonText.setEnabled(false);
            //buttonText.setText(R.string.downloadStarted);//Set Button Text when download started
           // if (a=="print"){
               // Toast.makeText(context, "Printing..please wait..", Toast.LENGTH_LONG).show();
              //  a.equals(null);
            //}else{
                //Toast.makeText(context, "Starting Download..please wait..", Toast.LENGTH_LONG).show();
                //a.equals(null);
           // }

            // progressdialogue("Downloading..");

        }

        @Override
        protected void onPostExecute(Void result) {
            try {
                if (outputFile != null) {
                    //buttonText.setEnabled(true);
                    //buttonText.setText(R.string.downloadCompleted);//If Download completed then change button text
                   // if (a=="print"){
                      //  Toast.makeText(context, "Opening Printing Interface....", Toast.LENGTH_LONG).show();
                        //a.equals(null);
                    //}else{
                       // Toast.makeText(context, "Download Finished", Toast.LENGTH_LONG).show();
                       // a.equals(null);
                   // }
                    //Toast.makeText(context, "Download Fininshed", Toast.LENGTH_SHORT).show();
                    //Intent minten = new Intent(context, Pdfviews.class);
                    //minten.putExtra("file",downloadFileName);
                    //minten.setFlags(FLAG_ACTIVITY_NEW_TASK);
                    //context.startActivity(minten);
                    //Intent minten = new Intent(context,CustomPrintActivity.class);
                    //minten.putExtra("filename",downloadFileName);

                   PrintManager printManager = (PrintManager)getSystemService(Context.PRINT_SERVICE);
                   String jobName =getString(R.string.app_name) +
                           " Document";
                    printManager.print(jobName, new MyPrintDocumentAdapter(Printingactivity.this,downloadFileName),null);
                    //startActivity(minten);
                   // CustomPrintActivity mprintingclass = new CustomPrintActivity(context);
                    //mprintingclass.printss(downloadFileName);
                    /*new Thread(new Runnable() {
                        public void run() {
                            Uri path = Uri.fromFile(outputFile);
                            try {
                                Intent intent = new Intent(Intent.ACTION_VIEW);
                                intent.setDataAndType(path, "application/pdf");
                                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                context.startActivity(intent);
                               // finish();
                            } catch (ActivityNotFoundException e) {
                                Toast.makeText(context, "No Pdf Reader Application Installed", Toast.LENGTH_SHORT).show();
                            }
                        }
                    }).start();*/
                     return;
                } else {
                    //buttonText.setText(R.string.downloadFailed);//If download failed change button text
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            // buttonText.setEnabled(true);
                            // buttonText.setText(R.string.downloadAgain);//Change button text again after 3sec
                            Toast.makeText(context, "Print Failed..Try again", Toast.LENGTH_SHORT).show();
                        }
                    }, 3000);

                    Log.e(TAG, "Download Failed");

                }
            } catch (Exception e) {
                e.printStackTrace();

                //Change button text if exception occurs
                // buttonText.setText(R.string.downloadFailed);
                Toast.makeText(context, "Print Failed..Try again", Toast.LENGTH_SHORT).show();
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        //  buttonText.setEnabled(true);
                        //buttonText.setText(R.string.downloadAgain);
                        Toast.makeText(context, "Print Failed..Try again", Toast.LENGTH_SHORT).show();
                    }
                }, 3000);
                Log.e(TAG, "Download Failed with Exception - " + e.getLocalizedMessage());

            }


            super.onPostExecute(result);
        }  @Override
        protected Void doInBackground(Void... arg0) {
            try {
                URL url = new URL(downloadUrl);//Create Download URl
                HttpURLConnection c = (HttpURLConnection) url.openConnection();//Open Url Connection
                c.setRequestMethod("GET");//Set Request Method to "GET" since we are grtting data
                c.connect();//connect the URL Connection

                //If Connection response is not OK then show Logs
                if (c.getResponseCode() != HttpURLConnection.HTTP_OK) {
                    Log.e(TAG, "Server returned HTTP " + c.getResponseCode()
                            + " " + c.getResponseMessage());

                }


                //Get File if SD card is present
                if (new Checkforsdcard().sdcardispresent()) {

                    apkStorage = new File(
                            Environment.getExternalStorageDirectory() + "/"
                                    + Utils.downloadDirectory);
                } else
                    Toast.makeText(context, "Oops!! There is no SD Card.", Toast.LENGTH_SHORT).show();

                //If File is not present create directory
                if (!apkStorage.exists()) {
                    apkStorage.mkdir();
                    Log.e(TAG, "Directory Created.");
                }

                outputFile = new File(Environment.getExternalStorageDirectory(), downloadFileName);//Create Output file in Main File

                //Create New File if not present
                if (!outputFile.exists()) {
                    outputFile.createNewFile();
                    Log.e(TAG, "File Created");
                }

                FileOutputStream fos = new FileOutputStream(outputFile);//Get OutputStream for NewFile Location

                InputStream is = c.getInputStream();//Get InputStream for connection

                byte[] buffer = new byte[1024];//Set buffer type
                int len1 = 0;//init length
                while ((len1 = is.read(buffer)) != -1) {
                    fos.write(buffer, 0, len1);//Write new file
                }

                //Close all connection after doing task
                fos.close();
                is.close();

            } catch (Exception e) {

                //Read exception if something went wrong
                e.printStackTrace();
                outputFile = null;
                Log.e(TAG, "Download Error Exception " + e.getMessage());
            }

            return null;
        }
    }

    /*public void progressdialogue(String messages){
        mdialgoue.setMessage(messages);
        mdialgoue.setIndeterminate(false);
        mdialgoue.setCancelable(false);
        mdialgoue.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        mdialgoue.setProgress(10);
        mdialgoue.show();
        Handler mahand = new Handler();
        mahand.postDelayed(new Runnable() {
            @Override
            public void run() {
                mdialgoue.dismiss();
            }
        },3000);
    }*/
}
