package com.pracasinfosys.duhabi.app.Activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.basgeekball.awesomevalidation.AwesomeValidation;
import com.pracasinfosys.duhabi.app.MainActivity;

import com.pracasinfosys.duhabi.app.R;
import com.pracasinfosys.duhabi.app.Sessionmanagement;

import java.util.HashMap;
import java.util.Map;

import static com.basgeekball.awesomevalidation.ValidationStyle.COLORATION;


public class Registeractivity extends AppCompatActivity {
    EditText medittextphone,edittextname;
    Sessionmanagement msession;
    Button mbtnsend,mbtncanel;
    RequestQueue mrequest;
    String usename,phone;
    ProgressDialog mdialogue;
    AwesomeValidation mAwesomvalidation = new AwesomeValidation(COLORATION);
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registeractivity);
        mAwesomvalidation.setColor(Color.RED);
        msession = new Sessionmanagement(this);
        mrequest = Volley.newRequestQueue(this);
        mdialogue = new ProgressDialog(this);
        medittextphone = (EditText)findViewById(R.id.phone);
        // mAwesomvalidation.addValidation(Registeractivity.this, R.id.phone,"[0-9]+", R.string.err_name);
        edittextname = (EditText)findViewById(R.id.username);
        //mAwesomvalidation.addValidation(Registeractivity.this, R.id.username, "[a-zA-Z\\s]+", R.string.err_name);

        mbtncanel = (Button)findViewById(R.id.buttoncancel);
        mbtnsend =(Button)findViewById(R.id.buttonsend);
        mbtnsend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                usename = edittextname.getText().toString();
                phone = medittextphone.getText().toString();
                //mAwesomvalidation.validate();

                if(usename.length()>0){
                    // Toast.makeText(Registeractivity.this, "You cann't leave the filed Empty", Toast.LENGTH_SHORT).show();
                    edittextname.setBackgroundResource(R.drawable.edittext_outerline);
                }else{
                    edittextname.setBackgroundResource(R.drawable.edittext_errorouterline);
                }
                if(phone.length()==10){
                    medittextphone.setBackgroundResource(R.drawable.edittext_outerline);
                }else{
                    medittextphone.setBackgroundResource(R.drawable.edittext_errorouterline);
                    //Toast.makeText(Registeractivity.this, "Invalid Phone No.", Toast.LENGTH_SHORT).show();
                }
                if(usename.length()>0 && phone.length() ==10){
                    progressdialogue("Authenticating...");
                    if(isconnectd()){
                        msession.createloginsession(usename, phone);
                        voleyrequest();
                        mdialogue.dismiss();
                        Intent mintent = new Intent(Registeractivity.this, MainActivity.class);
                        startActivity(mintent);
                        finish();
                        medittextphone.setText("");

                        edittextname.setText("");
                        medittextphone.setBackgroundResource(R.drawable.edittext_outerline);
                        edittextname.setBackgroundResource(R.drawable.edittext_outerline);

                    }else{
                        mdialogue.dismiss();
                        Toast.makeText(Registeractivity.this, "No Internet Connection!!! Please Try Again Later...", Toast.LENGTH_SHORT).show();
                    }

                }else{
                    Toast.makeText(Registeractivity.this, "Invalid Phone No.", Toast.LENGTH_SHORT).show();
                }
                /*if(phone.length()== 10) {
                    //Toast.makeText(Registeractivity.this, "Please Enter Valid Phone No.", Toast.LENGTH_SHORT).show();
                  //  medittextphone.setBackgroundResource(R.drawable.edittext_errorouterline);

                    msession.createloginsession(usename, phone);
                    voleyrequest();
                    mdialogue.dismiss();
                    Intent mintent = new Intent(Registeractivity.this, MainActivity.class);
                    startActivity(mintent);
                    finish();
                    medittextphone.setText("");

                    edittextname.setText("");
                    medittextphone.setBackgroundResource(R.drawable.edittext_outerline);
                }else {
                    mdialogue.dismiss();
                    Toast.makeText(Registeractivity.this, "Invalid Phone No:", Toast.LENGTH_SHORT).show();
                    medittextphone.setBackgroundResource(R.drawable.edittext_errorouterline);
                }
                }else {
                    mdialogue.dismiss();
                   // Toast.makeText(Registeractivity.this, "Invalid Phone No:", Toast.LENGTH_SHORT).show();
                    edittextname.setBackgroundResource(R.drawable.edittext_errorouterline);
                    medittextphone.setBackgroundResource(R.drawable.edittext_errorouterline);

                }*/

            }
        });
        mbtncanel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //mAwesomvalidation.clear();
                finish();
            }
        });

    }
    public boolean isconnectd(){
        ConnectivityManager mconn = (ConnectivityManager)getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo minfo =mconn.getActiveNetworkInfo();
        if(minfo != null && minfo.isConnected()){
            return true;
        }
        return false;
    }
    public void progressdialogue(String messages){

        //mdialogue = new ProgressDialog(this);
        mdialogue.setMessage(messages);
        mdialogue.setIndeterminate(false);
        mdialogue.setCancelable(false);
        mdialogue.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        mdialogue.setProgress(10);
        mdialogue.show();
        Handler mahand = new Handler();
        mahand.postDelayed(new Runnable() {
            @Override
            public void run() {
                mdialogue.dismiss();
            }
        },4000);
    }
    public void voleyrequest(){

        //Log.d("v", "voleyrequest: nnnnnnnnnnnn"+name+sub+body+phone+address);
        String url = "http://pracasinfosys.com/duhabi/api/app_user";
        StringRequest postRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d("success", "onResponse: sssssssssssss"+response.toString());
                //  mdialogue.dismiss();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("fai", "onResponse: sssssssssssss"+error.toString());
            }
        }){
            @Override
            protected Map<String, String> getParams()
            {
                Map<String, String>  params = new HashMap<String, String>();
                params.put("name", usename);
                params.put("phone", phone);

                return params;
            }
        };
        mrequest.add(postRequest);
    }
}
