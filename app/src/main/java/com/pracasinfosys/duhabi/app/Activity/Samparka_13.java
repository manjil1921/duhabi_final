package com.pracasinfosys.duhabi.app.Activity;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.pracasinfosys.duhabi.app.Adapter.Recyclerview_samparka;
import com.pracasinfosys.duhabi.app.Interface_retrofit;
import com.pracasinfosys.duhabi.app.Model.Model_notice_array;
import com.pracasinfosys.duhabi.app.Model.Model_notice_main;
import com.pracasinfosys.duhabi.app.R;
import com.pracasinfosys.duhabi.app.Restapi.Apiclient;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Samparka_13 extends AppCompatActivity {
    Interface_retrofit minterface;
    RecyclerView mrecycler;
    RecyclerView.LayoutManager mlayoutmanager;
    Recyclerview_samparka madapter;
    SwipeRefreshLayout mswiperefresh;
    List<Model_notice_array> mlist_sarbajanik = new ArrayList<>();
    Toolbar mtolbars;
    String getoda;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_samparka_13);
        mtolbars = (Toolbar)findViewById(R.id.ToolBars);
        mrecycler=(RecyclerView)findViewById(R.id.mrecyclerview);
        mswiperefresh =(SwipeRefreshLayout)findViewById(R.id.mswiprefresh);
        mswiperefresh.setRefreshing(true);


        //names = getIntent().getExtras().getString("name_1");
        setSupportActionBar(mtolbars);
        //setTitle("सम्पर्क / लोकेसन म्याप");
        //getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDefaultDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
       getSupportActionBar().setDisplayShowTitleEnabled(false);
        mtolbars.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        getoda = getIntent().getExtras().getString("oda");
        mswiperefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                mswiperefresh.setRefreshing(true);
                if(isconnectd()){
                    getreestapi_sarbajanikkharid();
                    Toast.makeText(Samparka_13.this,"Update Successfull",Toast.LENGTH_SHORT).show();
                    mswiperefresh.setRefreshing(false);
                }else{
                    //mlinear.setVisibility(View.VISIBLE);
                    Toast.makeText(Samparka_13.this,"Failed",Toast.LENGTH_SHORT).show();
                    mswiperefresh.setRefreshing(false);
                }

            }
        });
        mlayoutmanager = new LinearLayoutManager(this,LinearLayoutManager.VERTICAL,false);
        mrecycler.setLayoutManager(mlayoutmanager);
        getreestapi_sarbajanikkharid();
    }
    public boolean isconnectd(){
        ConnectivityManager mconn = (ConnectivityManager)getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo minfo =mconn.getActiveNetworkInfo();
        if(minfo != null && minfo.isConnected()){
            return true;
        }
        return false;
    }
    public void getreestapi_sarbajanikkharid(){
        String ward_id = getoda;
        minterface = Apiclient.getApiclient().create(Interface_retrofit.class);
        Call<Model_notice_main> msarbajanik = minterface.mgetinfo(ward_id);
        msarbajanik.enqueue(new Callback<Model_notice_main>() {
            @Override
            public void onResponse(Call<Model_notice_main> call, Response<Model_notice_main> response) {
                mlist_sarbajanik = response.body().getResult();
                Log.d("suc", "onResponse: kkkkkkkkkkk"+mlist_sarbajanik);
                mrecycler.setAdapter(new Recyclerview_samparka(mlist_sarbajanik,getApplicationContext()));
                mswiperefresh.setRefreshing(false);
            }

            @Override
            public void onFailure(Call<Model_notice_main> call, Throwable t) {
                Log.d("failed", "onResponse: kkkkkkkkkkk"+t.toString());
            }
        });
    }
}
