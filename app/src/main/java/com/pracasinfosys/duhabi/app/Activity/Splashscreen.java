package com.pracasinfosys.duhabi.app.Activity;

import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ProgressBar;

import com.pracasinfosys.duhabi.app.MainActivity;
import com.pracasinfosys.duhabi.app.R;
import com.pracasinfosys.duhabi.app.Sessionmanagement;

import java.util.HashMap;

public class Splashscreen extends AppCompatActivity {
ProgressBar progress;
    Sessionmanagement msession;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splashscreen);
        progress =(ProgressBar)findViewById(R.id.mprogressbar);
        progress.setVisibility(View.VISIBLE);
        msession = new Sessionmanagement(this);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                //Intent intent = new Intent(Splashscreen.this, MainActivity.class);
                //startActivity(intent);
                //finish();
                if(msession.isLoggedIn()){
                    HashMap<String,String> user = msession.getuserdetails();
                    String name = user.get(Sessionmanagement.KEY_NAME);
                    //String fbid = user.get(Sessionmanagement.KEY_APPID);
                    String gender = user.get(Sessionmanagement.KEY_PHONE);
                    //String image = user.get(Sessionmanagement.KEY_IMAGE);
                    Intent i = new Intent(getApplicationContext(),MainActivity.class);
                    //i.putExtra("name",name);
                    //i.putExtra("appid",fbid);
                    //i.putExtra("gender",gender);
                    //i.putExtra("image",image);
                    startActivity(i);
                    finish();
                }else {
                    msession.checkloginstatus();
                    finish();

                }

            }
        }, 4000);
    }

}
