package com.pracasinfosys.duhabi.app.Activity;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Toast;


//import com.android.volley.Request;
//import com.android.volley.VolleyError;
//import com.android.volley.toolbox.JsonObjectRequest;
import com.pracasinfosys.duhabi.app.Adapter.Recyclerview_Suchanasamachar;
import com.pracasinfosys.duhabi.app.Interface_retrofit;
import com.pracasinfosys.duhabi.app.Model.Model_details_samachar;
import com.pracasinfosys.duhabi.app.Model.Model_notice_array;
import com.pracasinfosys.duhabi.app.Model.Model_notice_main;
import com.pracasinfosys.duhabi.app.R;
import com.pracasinfosys.duhabi.app.Restapi.Apiclient;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Suchanatathasamachar extends AppCompatActivity {
    Interface_retrofit minterface;
    RecyclerView mrecycler;
    RecyclerView.LayoutManager mlayoutmanager;
    Recyclerview_Suchanasamachar madapter;
    SwipeRefreshLayout mswiperefresh;
    List<Model_details_samachar> mlist = new ArrayList<>();
    List<Model_notice_array> mlistss = new ArrayList<>();
    String names;
    Toolbar mtolbars;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_suchanatathasamachar);
        mtolbars = (Toolbar)findViewById(R.id.ToolBars);
        mrecycler=(RecyclerView)findViewById(R.id.mrecyclerview);
        mswiperefresh =(SwipeRefreshLayout)findViewById(R.id.mswiprefresh);
         mswiperefresh.setRefreshing(true);

      //  getrestdata_samacharworker();
        //names = getIntent().getExtras().getString("name_1");
        setSupportActionBar(mtolbars);
        //setTitle("सुचना तथा समाचार");
        //getSupportActionBar().setDisplayHomeAsUpEnabled(true);
         getSupportActionBar().setDefaultDisplayHomeAsUpEnabled(true);
         getSupportActionBar().setDisplayHomeAsUpEnabled(true);
         getSupportActionBar().setDisplayShowTitleEnabled(false);
        //mswiperefresh.setRefreshing(true);

        if(isconnectd()){
            getrestdata_samachar();
            Toast.makeText(Suchanatathasamachar.this,"Refresh Successfull",Toast.LENGTH_SHORT).show();
            mswiperefresh.setRefreshing(false);
        }else{
            //mlinear.setVisibility(View.VISIBLE);
            Toast.makeText(Suchanatathasamachar.this,"Refresh Failed ",Toast.LENGTH_SHORT).show();
            readdata();
            mswiperefresh.setRefreshing(false);
        }
        mtolbars.setNavigationOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View view) {
        finish();
        }
        });
        mswiperefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                mswiperefresh.setRefreshing(true);
                if(isconnectd()){
                    getrestdata_samachar();
                    Toast.makeText(Suchanatathasamachar.this,"Refresh Successfull",Toast.LENGTH_SHORT).show();
                    mswiperefresh.setRefreshing(false);
                }else{
                    //mlinear.setVisibility(View.VISIBLE);
                    Toast.makeText(Suchanatathasamachar.this,"Refresh Failed ",Toast.LENGTH_SHORT).show();
                    readdata();
                    mswiperefresh.setRefreshing(false);
                }

            }
        });
        mlayoutmanager = new LinearLayoutManager(this,LinearLayoutManager.VERTICAL,false);
        //mlayoutmanager = new GridLayoutManager(this,2);
        mrecycler.setLayoutManager(mlayoutmanager);
        getrestdata_samachar();
    }
    public boolean isconnectd(){
        ConnectivityManager mconn = (ConnectivityManager)getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo minfo =mconn.getActiveNetworkInfo();
        if(minfo != null && minfo.isConnected()){
            return true;
        }
        return false;
    }
   /* public void getrestdata_samachar(){
        minterface =Apiclient.getApiclient().create(Interface_retrofit.class);
        Call<Model_suchanatathasamachar> mgetdatas=minterface.mgetnews();
        Log.d("failed", "onFailure: faileeeeeeeeeeeeeeeeeeeeeeeee");
        mgetdatas.enqueue(new Callback<Model_suchanatathasamachar>() {
            @Override
            public void onResponse(Call<Model_suchanatathasamachar> call, Response<Model_suchanatathasamachar> response) {
                  mlist = response.body().getResult();
                madapter = new Recyclerview_Suchanasamachar(mlist,getApplicationContext());
                mrecycler.setAdapter(madapter);
                Log.d("success", "onFailure: "+mlist);

            }

            @Override
            public void onFailure(Call<Model_suchanatathasamachar> call, Throwable t) {
                Log.d("failed", "onFailure: "+t.toString());
            }
        });
    }*/



   public void getrestdata_samachar(){
       minterface=Apiclient.getApiclient().create(Interface_retrofit.class);
           Call<Model_notice_main> mnotice = minterface.mgetnews();
           mnotice.enqueue(new Callback<Model_notice_main>() {
               @Override
               public void onResponse(Call<Model_notice_main> call, Response<Model_notice_main> response) {
                  mlistss = response.body().getResult();
                   Log.d("faileds", "onFailure: failesd"+mlistss);
                   //mrecycler.setAdapter(new Duhabi_newsadapter(getApplicationContext(),mlistss));
                   mrecycler.setAdapter(new Recyclerview_Suchanasamachar(mlistss,getApplicationContext()));
                   mswiperefresh.setRefreshing(false);
                   savecachedata();
               }

               @Override
               public void onFailure(Call<Model_notice_main> call, Throwable t) {
                   Log.d("failed", "onFailure: failesd"+t.toString());
               }
           });


   }
   public void savecachedata()  {
       File folder = getCacheDir();
       File myfile = new File(folder,"samachar.txt");
       FileOutputStream fileoutputstream = null;
       try {
           fileoutputstream = new FileOutputStream(myfile);
       } catch (FileNotFoundException e) {
           e.printStackTrace();
       }
       try {
           fileoutputstream.write(mlistss.toString().getBytes());
           //Toast.makeText(this, "Sucessfulle Cached Data", Toast.LENGTH_SHORT).show();
       } catch (IOException e) {
           e.printStackTrace();
       }
       finally {
           if (fileoutputstream!=null){
               try {
                   fileoutputstream.close();
               } catch (IOException e) {
                   e.printStackTrace();
               }
           }

       }
   }
   public void readdata(){
       File folders = getCacheDir();
       File myfile = new File(folders,"samachar.txt");
       String marays = offiline_readdata(myfile);
       //mlistss = marays.getBytes();

       if(marays.length()>0){
          // Toast.makeText(this, "your datsa is "+marays, Toast.LENGTH_SHORT).show();

       }else {
          // Toast.makeText(this, "No data in cache", Toast.LENGTH_SHORT).show();
       }

   }
   public String offiline_readdata(File myfile) {
       FileInputStream fs = null;
       try {
           fs = new FileInputStream(myfile);
       } catch (FileNotFoundException e) {
           e.printStackTrace();
       }
       int read = -1;
       StringBuffer mbuffer = new StringBuffer();
       try {
           while ((read = fs.read()) != -1) {
               mbuffer.append((char) read);
           }
           return mbuffer.toString();
       } catch (IOException e) {
           e.printStackTrace();
       }


     finally{
           if(fs!=null){
               try {
                   fs.close();
               } catch (IOException e) {
                   e.printStackTrace();
               }

           }
       }
   return null;
   }
//  public void getrestdata_samacharworker(){
//        minterface= Apiii.getApiclient().create(Interface_retrofit.class);
//        Call<Model_notice_main> mnotice = minterface.mgetworkders("1");
//        mnotice.enqueue(new Callback<Model_notice_main>() {
//            @Override
//            public void onResponse(Call<Model_notice_main> call, Response<Model_notice_main> response) {
//                Log.d("p", "onResponse:  dddddddddddddd"+response.body().getResult());
//            }
//
//            @Override
//            public void onFailure(Call<Model_notice_main> call, Throwable t) {
//
//            }
//        });

    //}
}
