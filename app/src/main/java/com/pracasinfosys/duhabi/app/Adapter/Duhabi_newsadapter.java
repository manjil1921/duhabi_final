package com.pracasinfosys.duhabi.app.Adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.pracasinfosys.duhabi.app.Activity.Newscontent_details;
import com.pracasinfosys.duhabi.app.Model.Model_notice_array;
import com.pracasinfosys.duhabi.app.R;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by Dell on 2/5/2018.
 */

public class Duhabi_newsadapter extends RecyclerView.Adapter<Duhabi_newsadapter.MyViewHolder> {
    private Context mcontext;
    List<Model_notice_array> mdetails;

    public Duhabi_newsadapter(Context mcontext, List<Model_notice_array> mdetails) {
        this.mcontext = mcontext;
        this.mdetails = mdetails;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View mview = LayoutInflater.from(parent.getContext()).inflate(R.layout.cardview_newsfeeddesign,parent,false);
        return new MyViewHolder(mview);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
    Model_notice_array mgetnews = mdetails.get(position);
    holder.mtextview.setText(mgetnews.getTitle());
        Picasso.with(mcontext).load(mgetnews.getImage()).into(holder.getimage);
        holder.mtextview_date.setText(mgetnews.getDate());
    }

    @Override
    public int getItemCount() {
        return mdetails.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        TextView mtextview,mtextview_date;
        ImageView getimage;
        public MyViewHolder(View itemView) {
            super(itemView);
            mtextview = (TextView)itemView.findViewById(R.id.textview_news);
            getimage =(ImageView)itemView.findViewById(R.id.imageview_overlay);
            mtextview_date = (TextView)itemView.findViewById(R.id.textview_datesss);
            mtextview.setOnClickListener(this);
            getimage.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            //Toast.makeText(mcontext, "Your clicked on", Toast.LENGTH_SHORT).show();
            int id = getAdapterPosition();
            Intent mintent = new Intent(view.getContext(), Newscontent_details.class);
            mintent.putExtra("image",mdetails.get(id).getImage());
            mintent.putExtra("title",mdetails.get(id).getTitle());
            mintent.putExtra("dates",mdetails.get(id).getDate());
            mintent.putExtra("detailcontent",mdetails.get(id).getContent());
            view.getContext().startActivity(mintent);

        }
    }
}
