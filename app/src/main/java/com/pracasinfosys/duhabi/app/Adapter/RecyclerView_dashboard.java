package com.pracasinfosys.duhabi.app.Adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;


import com.pracasinfosys.duhabi.app.Activity.Byaktikatghatana_4;
import com.pracasinfosys.duhabi.app.Activity.Gunasotathasujhav_8;
import com.pracasinfosys.duhabi.app.Activity.Kartathasulka_6;
import com.pracasinfosys.duhabi.app.Activity.Mahatyopurnanumber_14;
import com.pracasinfosys.duhabi.app.Activity.Main2Activity;
import com.pracasinfosys.duhabi.app.Activity.Nagarikbadapatra;
import com.pracasinfosys.duhabi.app.Activity.Nagariksurakshya_10;
import com.pracasinfosys.duhabi.app.Activity.Nibedankodhacha;
import com.pracasinfosys.duhabi.app.Activity.Padadhikairkobibaran_12;
import com.pracasinfosys.duhabi.app.Activity.Patrachargarnuhos_7;
import com.pracasinfosys.duhabi.app.Activity.Samajiksurakshya_details;
import com.pracasinfosys.duhabi.app.Activity.Samparka_13;
import com.pracasinfosys.duhabi.app.Activity.Sarbajanickkharid_2;
import com.pracasinfosys.duhabi.app.Activity.Upavoktasamiti_1;
import com.pracasinfosys.duhabi.app.Model.Model_nagarpalikadashboard;
import com.pracasinfosys.duhabi.app.R;
import com.squareup.picasso.Picasso;
//import com.example.razu.egovernanceapp.R;
import java.util.List;

/**
 * Created by Razu on 1/1/2018.
 */

public class RecyclerView_dashboard extends RecyclerView.Adapter<RecyclerView_dashboard.MyViewHolder> {
    List<Model_nagarpalikadashboard> mlists;
    private Context mcontext;

    public RecyclerView_dashboard(List<Model_nagarpalikadashboard> mlists, Context mcontext) {
        this.mlists = mlists;
        this.mcontext = mcontext;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.cardview_dashboard,parent,false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        Model_nagarpalikadashboard images = mlists.get(position);
        holder.mtextview.setText(images.getText());
        Picasso.with(mcontext).load(images.getImage()).into(holder.imagevies);
    }

    @Override
    public int getItemCount() {
        return mlists.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        ImageView imagevies;
        TextView mtextview;
        //TextView fontAwesomeAndroidIcon;
        public MyViewHolder(View itemView) {
            super(itemView);
            imagevies =(ImageView)itemView.findViewById(R.id.imageview_image_sem);
            mtextview =(TextView)itemView.findViewById(R.id.textview_title_sem);
            imagevies.setOnClickListener(this);
            //Typeface fontAwesomeFont = Typeface.createFromAsset(itemView.getContext().getAssets(), "fontawesome-webfont.ttf");
            //fontAwesomeAndroidIcon = (TextView)itemView.findViewById(R.id.font_awesome_android_icon);
            //fontAwesomeAndroidIcon.setTypeface(fontAwesomeFont);
            //fontAwesomeAndroidIcon.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            int id = getAdapterPosition();
            switch (id){
                case 0:
                    Intent mintent0 = new Intent(view.getContext(), Main2Activity.class);
                   // mintent0.putExtra("names_1","सुचना तथा समाचार");
                    view.getContext().startActivity(mintent0);
                    break;
                case 1:
                    Intent mintent1 = new Intent(view.getContext(), Sarbajanickkharid_2.class);
                    view.getContext().startActivity(mintent1);
                    break;
                case 2:
                    Intent mintent2 = new Intent(view.getContext(), Nagarikbadapatra.class);
                    view.getContext().startActivity(mintent2);
                    break;
                case 3:
                    Intent mintent3 = new Intent(view.getContext(), Byaktikatghatana_4.class);
                    view.getContext().startActivity(mintent3);
                    break;
                case 4:
                    Intent mintent4 = new Intent(view.getContext(), Samajiksurakshya_details.class);
                    view.getContext().startActivity(mintent4);
                    break;
                case 5:
                    Intent mintent5 = new Intent(view.getContext(), Kartathasulka_6.class);
                    view.getContext().startActivity(mintent5);
                    break;
                case 6:
                    Intent mintent6 = new Intent(view.getContext(), Nibedankodhacha.class);
                    view.getContext().startActivity(mintent6);
                    break;
                case 7:
                    Intent mintent7 = new Intent(view.getContext(), Patrachargarnuhos_7.class);
                    view.getContext().startActivity(mintent7);
                    break;
                case 8:
                    Intent mintent8 = new Intent(view.getContext(), Gunasotathasujhav_8.class);
                    view.getContext().startActivity(mintent8);
                    break;
                case 9:
                    Intent mintent9 = new Intent(view.getContext(), Nagariksurakshya_10.class);
                    view.getContext().startActivity(mintent9);
                    break;
                case 10:
                    Intent mintent10 = new Intent(view.getContext(), Upavoktasamiti_1.class);
                    view.getContext().startActivity(mintent10);
                    break;
                case 11:
                    Intent mintent11 = new Intent(view.getContext(), Padadhikairkobibaran_12.class);
                    view.getContext().startActivity(mintent11);
                    break;
                case 12:
                    Intent mintent12 = new Intent(view.getContext(), Samparka_13.class);
                    view.getContext().startActivity(mintent12);
                    break;
                case 13:
                    Intent mintent13 = new Intent(view.getContext(), Mahatyopurnanumber_14.class);
                    view.getContext().startActivity(mintent13);
                    break;

            }
        }
    }
}
