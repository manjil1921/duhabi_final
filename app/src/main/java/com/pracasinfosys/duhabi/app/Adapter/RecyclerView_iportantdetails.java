package com.pracasinfosys.duhabi.app.Adapter;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.support.v4.app.ActivityCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.pracasinfosys.duhabi.app.Model.Model_notice_array;
import com.pracasinfosys.duhabi.app.R;

import java.util.List;

/**
 * Created by user on 1/19/2018.
 */

public class RecyclerView_iportantdetails extends RecyclerView.Adapter<RecyclerView_iportantdetails.MyViewHolder> {

   private Context mcontext;
   List<Model_notice_array> mgetararys;

    public RecyclerView_iportantdetails(Context mcontext, List<Model_notice_array> mgetararys) {
        this.mcontext = mcontext;
        this.mgetararys = mgetararys;

    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View mview = LayoutInflater.from(parent.getContext()).inflate(R.layout.cardiview_importantnumber,parent,false);

        return new MyViewHolder(mview);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
      Model_notice_array marrays = mgetararys.get(position);
      holder.mphoneno.setText(marrays.getMob());
      holder.morganizationname.setText(marrays.getName());

    }

    @Override
    public int getItemCount() {
        return mgetararys.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView morganizationname,mphoneno;
        public MyViewHolder(View itemView) {
            super(itemView);
            morganizationname = (TextView)itemView.findViewById(R.id.mname);
            mphoneno = (TextView)itemView.findViewById(R.id.textview_phone);
            mphoneno.setOnClickListener(this);

        }

        @Override
        public void onClick(View view) {
            int id = getAdapterPosition();
            String dial = "tel:" + mgetararys.get(id).getMob();
            Intent callIntents = new Intent(Intent.ACTION_CALL);
            callIntents.setData(Uri.parse(dial));
            if (ActivityCompat.checkSelfPermission(mcontext, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
                return;

            }
            view.getContext().startActivity(callIntents);
        }
    }
}
