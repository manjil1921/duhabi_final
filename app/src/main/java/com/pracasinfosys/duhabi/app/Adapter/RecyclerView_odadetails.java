package com.pracasinfosys.duhabi.app.Adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.pracasinfosys.duhabi.app.Activity.Byaktikatghatana_4;
import com.pracasinfosys.duhabi.app.Activity.Education_activity;
import com.pracasinfosys.duhabi.app.Activity.Nagarikbadapatra;
import com.pracasinfosys.duhabi.app.Activity.Padadhikairkobibaran_12;
import com.pracasinfosys.duhabi.app.Activity.Samajiksurakshya_5;
import com.pracasinfosys.duhabi.app.Activity.Samparka_13;
import com.pracasinfosys.duhabi.app.Activity.Swasthay_activity;
import com.pracasinfosys.duhabi.app.Model.Model_notice_array;
import com.pracasinfosys.duhabi.app.R;

import java.util.List;

/**
 * Created by Paru on 2/22/2018.
 */

public class RecyclerView_odadetails extends RecyclerView.Adapter<RecyclerView_odadetails.MyViewHolder> {
    private Context mcontext;
    List<Model_notice_array> mgetarray;
    String activity;

    public RecyclerView_odadetails(Context mcontext, List<Model_notice_array> mgetarray,String activity) {
        this.mcontext = mcontext;
        this.mgetarray = mgetarray;
        this.activity = activity;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
     View mview = LayoutInflater.from(parent.getContext()).inflate(R.layout.activity_list,parent,false);

        return new MyViewHolder(mview);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
     Model_notice_array mgetlist = mgetarray.get(position);
     holder.mtextview.setText(mgetlist.getName());
    }

    @Override
    public int getItemCount() {
        return mgetarray.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView mtextview;
        public MyViewHolder(View itemView) {
            super(itemView);
        mtextview=(TextView)itemView.findViewById(R.id.textview_details_woda1);
        mtextview.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
          int getid = getAdapterPosition();
          int getidss =getid +1;
          String getids = Integer.toString(getidss);
          if(activity.equals("shikshya")){
              Intent minten = new Intent(v.getContext(), Education_activity.class);
              minten.putExtra("oda",mgetarray.get(getid).getId());
              v.getContext().startActivity(minten);

          }else if(activity.equals("health")){
              Intent minten = new Intent(v.getContext(), Swasthay_activity.class);
              minten.putExtra("oda",mgetarray.get(getid).getId());
              v.getContext().startActivity(minten);


          }
          else if(activity.equals("padhadhikari")){
              Intent minten = new Intent(v.getContext(), Padadhikairkobibaran_12.class);
              minten.putExtra("oda",mgetarray.get(getid).getId());
              v.getContext().startActivity(minten);


          }
          else if(activity.equals("samparka")){
              Intent minten = new Intent(v.getContext(), Samparka_13.class);
              minten.putExtra("oda",mgetarray.get(getid).getId());
              v.getContext().startActivity(minten);


          }
          else if(activity.equals("byaktigat")){
              Intent minten = new Intent(v.getContext(), Byaktikatghatana_4.class);
              minten.putExtra("oda",mgetarray.get(getid).getId());
              v.getContext().startActivity(minten);


          }
          else if(activity.equals("NagarikBadapatra")){
              Intent minten = new Intent(v.getContext(), Nagarikbadapatra.class);
              minten.putExtra("oda",mgetarray.get(getid).getId());
              v.getContext().startActivity(minten);


          }
          else{
              Intent minten = new Intent(v.getContext(), Samajiksurakshya_5.class);
              minten.putExtra("oda",mgetarray.get(getid).getId());
              v.getContext().startActivity(minten);

          }

        }
    }
}
