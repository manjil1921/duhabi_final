package com.pracasinfosys.duhabi.app.Adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.pracasinfosys.duhabi.app.Activity.Downloadable_neewactivity;
import com.pracasinfosys.duhabi.app.Activity.Sarbajanik_2_details;
import com.pracasinfosys.duhabi.app.Model.Model_notice_array;
import com.pracasinfosys.duhabi.app.R;
import com.pracasinfosys.duhabi.app.Utils;

import java.util.List;

/**
 * Created by Dell on 2/21/2018.
 */

public class RecyclerView_updated extends RecyclerView.Adapter<RecyclerView_updated.MyViewHolder> {
    private Context mcontext;
    public List<Model_notice_array> mgetlist;
    String name;
    // int i;

    public RecyclerView_updated(Context mcontext, List<Model_notice_array> mgetlist,String name) {
        this.mcontext = mcontext;
        this.mgetlist = mgetlist;
        this.name = name;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View mview = LayoutInflater.from(parent.getContext()).inflate(R.layout.cardview_updated,parent,false);
        return new MyViewHolder(mview);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        Model_notice_array marray = mgetlist.get(position);
        holder.mtitle.setText(marray.getTitle());
        // i = marray.getType();
        if(name.equals("badapatra")){

            holder.mdescription.setText(marray.getDescription());

        }else{
            holder.mdescription.setText(marray.getContent());

        }


    }

    @Override
    public int getItemCount() {
        return mgetlist.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        TextView mtitle,mdescription;
        public MyViewHolder(View itemView) {
            super(itemView);
            mtitle = (TextView)itemView.findViewById(R.id.textview_title);
            mdescription =(TextView)itemView.findViewById(R.id.textview_description);
            mtitle.setOnClickListener(this);
            mdescription.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            int id = getAdapterPosition();
            String murl;
            if(mgetlist.get(id).getFile().equals("")){
                murl = "nofile";
                //Log.d("c", "onClick: jjjjjjjjjjjjjjjjjjjjjjjjjj++++"+murl+"filetype"+i);
                //// Toast.makeText(mcontext, "null file type"+murl+"filetype"+mgetlist.get(id).getType(), Toast.LENGTH_LONG).show();

            }else{
                murl = mgetlist.get(id).getFile();
                // Log.d("c", "onClick: jjjjjjjjjjjjjjjjjjjjjjjjjj++++_filehaving"+murl+"filetype"+i);
                //Toast.makeText(mcontext, "null file type"+murl+"filetype"+mgetlist.get(id).getType(), Toast.LENGTH_LONG).show();

            }
            String filess ;
            if(name.equals("samajiksurakshya")){
                String downloadFileName = murl.replace(Utils.mainUrl_socialssecurity, "");
                Log.d("bb", "onClick: ssssssssssssssssmmmmmmmmm"+downloadFileName);

                Intent mintent = new Intent(view.getContext(), Downloadable_neewactivity.class);
                mintent.putExtra("title",mgetlist.get(id).getTitle());
                mintent.putExtra("description",mgetlist.get(id).getContent());
                mintent.putExtra("tolbar_text","सामाजिक सुरक्षा");
                mintent.putExtra("fileurl",murl);
                mintent.putExtra("id",id);
                mintent.putExtra("type",mgetlist.get(id).getType());
                mintent.putExtra("downloadfile",downloadFileName);
                mintent.putExtra("printfilename","samajiksurakshya_");
                view.getContext().startActivity(mintent);

            }
            else if(name.equals("edu")){
                String downloadFileName = murl.replace(Utils.mainUrl_education, "");
                Log.d("bb", "onClick: ssssssssssssssssmmmmmmmmm"+downloadFileName);

                Intent mintent = new Intent(view.getContext(), Downloadable_neewactivity.class);
                mintent.putExtra("title",mgetlist.get(id).getTitle());
                mintent.putExtra("description",mgetlist.get(id).getContent());
                mintent.putExtra("tolbar_text","शिक्षा");
                mintent.putExtra("fileurl",murl);
                mintent.putExtra("id",id);
                mintent.putExtra("type",mgetlist.get(id).getType());
                mintent.putExtra("downloadfile",downloadFileName);
                mintent.putExtra("printfilename","edu");
                view.getContext().startActivity(mintent);

            }
            else if(name.equals("swasthya")){
                String downloadFileName = murl.replace(Utils.mainUrl_health, "");
                Log.d("bb", "onClick: ssssssssssssssssmmmmmmmmm"+downloadFileName);

                Intent mintent = new Intent(view.getContext(), Downloadable_neewactivity.class);
                mintent.putExtra("title",mgetlist.get(id).getTitle());
                mintent.putExtra("description",mgetlist.get(id).getContent());
                mintent.putExtra("tolbar_text","स्वास्थ्य");
                mintent.putExtra("fileurl",murl);
                mintent.putExtra("id",id);
                mintent.putExtra("type",mgetlist.get(id).getType());
                mintent.putExtra("downloadfile",downloadFileName);
                mintent.putExtra("printfilename","shikshya");
                view.getContext().startActivity(mintent);

            }


            else if(name.equals("upovokta")){
                String downloadFileName = murl.replace(Utils.mainUrl_consumer, "");
                Log.d("bb", "onClick: ssssssssssssssssmmmmmmmmm"+downloadFileName);

                Intent mintent = new Intent(view.getContext(), Downloadable_neewactivity.class);
                mintent.putExtra("title",mgetlist.get(id).getTitle());
                mintent.putExtra("description",mgetlist.get(id).getContent());
                mintent.putExtra("tolbar_text","उपभोक्ता समिती");
                mintent.putExtra("fileurl",mgetlist.get(id).getFile());
                mintent.putExtra("id",id);
                mintent.putExtra("type",mgetlist.get(id).getType());
                mintent.putExtra("downloadfile",downloadFileName);
                mintent.putExtra("printfilename","upvokta_");
                view.getContext().startActivity(mintent);

            }else if(name.equals("nagarik")){
                String downloadFileName = murl.replace(Utils.mainUrl_personalsecurity, "");
                Log.d("bb", "onClick: ssssssssssssssssmmmmmmmmm"+downloadFileName);
                Intent mintent = new Intent(view.getContext(), Downloadable_neewactivity.class);
                mintent.putExtra("title",mgetlist.get(id).getTitle());
                mintent.putExtra("description",mgetlist.get(id).getContent());
                mintent.putExtra("tolbar_text","सामाजिक सुरक्षा फारम");
                mintent.putExtra("fileurl",murl);
                mintent.putExtra("downloadfile",downloadFileName);
                mintent.putExtra("id",id);
                mintent.putExtra("type",mgetlist.get(id).getType());
                mintent.putExtra("printfilename","social_security_");
                view.getContext().startActivity(mintent);

            }
//            else if(name.equals("parichaya")){
//                String downloadFileName = murl.replace(Utils.mainUrl_personalsecurity, "");
//                Log.d("bb", "onClick: ssssssssssssssssmmmmmmmmm"+downloadFileName);
//                Intent mintent = new Intent(view.getContext(), Downloadable_neewactivity.class);
//                mintent.putExtra("title",mgetlist.get(id).getTitle());
//                mintent.putExtra("description",mgetlist.get(id).getContent());
//                mintent.putExtra("tolbar_text","नागरिक सुरक्षा फारम");
//                mintent.putExtra("fileurl",mgetlist.get(id).getFile());
//                mintent.putExtra("downloadfile",downloadFileName);
//                mintent.putExtra("id",id);
//                mintent.putExtra("type",i);
//                mintent.putExtra("printfilename","personalsecurity_");
//                view.getContext().startActivity(mintent);
//
//            }

            else{

                if(mgetlist.get(id).getFile().equals("")){
                    filess = "Nofile";

                }else{
                    filess = mgetlist.get(id).getFile();
                }
                //Toast.makeText(mcontext, "Please Select an Item First", Toast.LENGTH_SHORT).show();
                //String downloadFileName = murl.replace(Utils.mainUrl_personalsecurity, "");
                // Log.d("bb", "onClick: ssssssssssssssssmmmmmmmmm"+downloadFileName);
                Intent mintent = new Intent(view.getContext(), Sarbajanik_2_details.class);
                ///mintent.putExtra("title",mgetlist.get(id).getTitle());
                mintent.putExtra("content",mgetlist.get(id).getDescription());
                mintent.putExtra("tolbar_text","नागरिक वडापत्र");
                mintent.putExtra("file", filess);
                mintent.putExtra("downloadfile","Nagarikbadapatra.pdf");
                mintent.putExtra("type",mgetlist.get(id).getType());
                //mintent.putExtra("id",id);
                //mintent.putExtra("printfilename","badapatra_");
                view.getContext().startActivity(mintent);
            }

        }
    }
}
