package com.pracasinfosys.duhabi.app.Adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.pracasinfosys.duhabi.app.Activity.Newscontent_details;
import com.pracasinfosys.duhabi.app.Model.Model_notice_array;
import com.pracasinfosys.duhabi.app.R;

import java.util.List;

/**
 * Created by user on 1/4/2018.
 */

public class Recyclerview_Suchanasamachar extends RecyclerView.Adapter<Recyclerview_Suchanasamachar.MyViewHolder> {
    List<Model_notice_array> mlist;
    private Context mcontext;


    public Recyclerview_Suchanasamachar(List<Model_notice_array> mlist, Context mcontext) {
        this.mlist = mlist;
        this.mcontext = mcontext;

    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.cardview_newscontent_1,parent,false);
        return new MyViewHolder(view);




    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
    Model_notice_array mdetails = mlist.get(position);
       //String texts= TextUtils.htmlEncode(mlist.get(position).getTitle());
      // holder.mtextviewtitle.setText(Html.fromHtml(texts));
        holder.mtextviewtitle.setText(mdetails.getTitle());
          holder.mtextviewdescription.setText(mdetails.getContent());
         holder.mtextviewdate.setText(mdetails.getDate());

    }

    @Override
    public int getItemCount() {
        return mlist.size();
    }

    public class MyViewHolder extends ViewHolder implements View.OnClickListener{
       TextView mtextviewtitle,mtextviewdescription,mtextviewdate;
        public MyViewHolder(View itemView) {
            super(itemView);

            mtextviewtitle =(TextView)itemView.findViewById(R.id.textview_1_newstitle);
            mtextviewdescription =(TextView)itemView.findViewById(R.id.textview_1_newsdescription);
            mtextviewdate =(TextView)itemView.findViewById(R.id.textview_1_newsdate);
            mtextviewtitle.setOnClickListener(this);
            mtextviewdescription.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            String imagess,filess;
            int id = getAdapterPosition();
            if(mlist.get(id).getFile().equals("")){
                filess = "No";

            }else{
                filess = mlist.get(id).getFile();
            }
            Log.d("ok", "onClick: tererererer+files"+filess);
            if(mlist.get(id).getImage().equals("")){
                imagess = "Nofile";

            }else{
                imagess = mlist.get(id).getImage();
            }


            Log.d("ok", "onClick: nnnnnnnnnnnnn0+image"+imagess);
            Intent mintent0 = new Intent(view.getContext(), Newscontent_details.class);
                    //mintent0.putExtra("names_1","सुचना तथा समाचार");
                    mintent0.putExtra("detailcontent",mlist.get(id).getContent());
                    mintent0.putExtra("dates",mlist.get(id).getDate());
                    mintent0.putExtra("title",mlist.get(id).getTitle());
                    mintent0.putExtra("iimage",imagess);
                    mintent0.putExtra("fil",filess);
                    mintent0.putExtra("type",mlist.get(id).getType());
                    view.getContext().startActivity(mintent0);


        }

    }
    public void dataread(int pos){
        TextUtils.htmlEncode(mlist.get(pos).getTitle());
       // YourTextview.setText(Html.fromHtml(jsStr));
    }
}
