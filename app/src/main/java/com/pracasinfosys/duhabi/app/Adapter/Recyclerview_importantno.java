package com.pracasinfosys.duhabi.app.Adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.pracasinfosys.duhabi.app.Activity.Detailsactivity;
import com.pracasinfosys.duhabi.app.Model.Model_notice_array;
import com.pracasinfosys.duhabi.app.R;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by user on 1/8/2018.
 */

public class Recyclerview_importantno extends RecyclerView.Adapter<Recyclerview_importantno.MyViewHolder> {
    List<Model_notice_array> mlist;
    private Context mcontext;

    public Recyclerview_importantno(List<Model_notice_array> mlist, Context mcontext) {
        this.mlist = mlist;
        this.mcontext = mcontext;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View mview = LayoutInflater.from(parent.getContext()).inflate(R.layout.cardiview_importantnumber1,parent,false);
        return new MyViewHolder(mview);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        Model_notice_array mdata = mlist.get(position);
        holder.mtextview_organizationname.setText(mdata.getName());
        Picasso.with(mcontext).load(mdata.getImage()).into(holder.morgani_name);

    }

    @Override
    public int getItemCount() {
        return mlist.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView mtextview_organizationname,mtextphone,mtextname;
        ImageView morgani_name;
        public MyViewHolder(View itemView) {
            super(itemView);
            mtextview_organizationname = (TextView)itemView.findViewById(R.id.organization);
            morgani_name = (ImageView)itemView.findViewById(R.id.mcricularview);
            //mtextphone = (TextView)itemView.findViewById(R.id.textview_phone);
            //mtextname=(TextView)itemView.findViewById(R.id.textview_name);
            //itemView.setOnClickListener(this);
            morgani_name.setOnClickListener(this);
            mtextview_organizationname.setOnClickListener(this);

            // mtextphone.setOnClickListener(this);


        }

        @Override
        public void onClick(View view) {
        int id = getAdapterPosition();
            //Toast.makeText(mcontext, "on click"+id, Toast.LENGTH_SHORT).show();
        //alertdialogue(mcontext,id);
            Intent minten = new Intent(view.getContext(), Detailsactivity.class);
           // minten.putExtra("name",mlist.get(id).getName());
           // minten.putExtra("post",mlist.get(id).getPost());
           // minten.putExtra("phone",mlist.get(id).getMob());
           // minten.putExtra("address",mlist.get(id).getAddress());
            minten.putExtra("officeid",mlist.get(id).getId());
            minten.putExtra("names",mlist.get(id).getName());
           view.getContext().startActivity(minten);
            /*String dial = "tel:" + mlist.get(id).getPhone();
            Intent callIntents = new Intent(Intent.ACTION_CALL);
            callIntents.setData(Uri.parse(dial));
            if (ActivityCompat.checkSelfPermission(mcontext, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
                return;

            }
            view.getContext().startActivity(callIntents);
        }*/
    }
    public void alertdialogue(Context mcontexts, final int position){
        AlertDialog.Builder dialoges = new AlertDialog.Builder(mcontexts);
        View viees = LayoutInflater.from(mcontexts).inflate(R.layout.activity_detailsactivity,null);
        //LayoutInflater minflae = new LayoutInflater(mcontext,R.layout.cardview_mahatono);
        final TextView name = (TextView)viees.findViewById(R.id.mname);
        final TextView starttimesss =(TextView) viees.findViewById(R.id.mtextview_post);
        final TextView phone = (TextView)viees.findViewById(R.id.textview_phone);
        final TextView address =(TextView) viees.findViewById(R.id.textview_address);
        name.setText(mlist.get(position).getName());
        starttimesss.setText(mlist.get(position).getPost());
        phone.setText(mlist.get(position).getPhone());
        address.setText(mlist.get(position).getAddress());
        dialoges.setView(viees);
        final AlertDialog dialoge = dialoges.create();
        dialoge.show();
       /* phone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String dial = "tel:"+ mlist.get(position).getPhone();
                Intent callIntent = new Intent(Intent.ACTION_CALL);
                callIntent.setData(Uri.parse(dial));
                if (ActivityCompat.checkSelfPermission(mcontext, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                    // TODO: Consider calling
                    //    ActivityCompat#requestPermissions
                    // here to request the missing permissions, and then overriding
                    //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                    //                                          int[] grantResults)
                    // to handle the case where the user grants the permission. See the documentation
                    // for ActivityCompat#requestPermissions for more details.
                    return;
                }
                view.getContext().startActivity(callIntent);
            }
        });*/


    }
}}

