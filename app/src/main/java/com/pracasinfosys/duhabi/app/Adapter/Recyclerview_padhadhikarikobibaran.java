package com.pracasinfosys.duhabi.app.Adapter;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.net.Uri;
import android.support.v4.app.ActivityCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.pracasinfosys.duhabi.app.Model.Model_notice_array;
import com.pracasinfosys.duhabi.app.R;
import com.mikhaellopez.circularimageview.CircularImageView;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by Razu on 1/6/2018.
 */

public class Recyclerview_padhadhikarikobibaran extends RecyclerView.Adapter<Recyclerview_padhadhikarikobibaran.MyViewHolder> {
    List<Model_notice_array> mlist;
    private Context mcontext;

    public Recyclerview_padhadhikarikobibaran(List<Model_notice_array> mlist, Context mcontext) {
        this.mlist = mlist;
        this.mcontext = mcontext;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View mview = LayoutInflater.from(parent.getContext()).inflate(R.layout.cardview_padhadhikarikobibaran, parent, false);
        return new MyViewHolder(mview);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        Model_notice_array marray = mlist.get(position);
        holder.mpost.setText(marray.getPost());
        //        holder.mwardno.setText(marray.getWard_no());
        holder.mphone.setText(marray.getPhone());
        if(marray.getEmail().equals("")){
            holder.memail.setText("उपलब्ध छैन");

        }else{
            holder.memail.setText(marray.getEmail());
        }
        holder.maddress.setText(marray.getAddress());
        holder.mname.setText(marray.getName());
        Picasso.with(mcontext).load(marray.getImage()).into(holder.mcricular);
    }

    @Override
    public int getItemCount() {
        return mlist.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        CircularImageView mcricular;
        TextView mname, maddress, memail, mphone, mwardno, mpost;
        LinearLayout mlinear;

        public MyViewHolder(View itemView) {
            super(itemView);
            mname = (TextView) itemView.findViewById(R.id.mname);
            maddress = (TextView) itemView.findViewById(R.id.textview_address);
            memail = (TextView) itemView.findViewById(R.id.textview_email);
            mphone = (TextView) itemView.findViewById(R.id.textview_phone);
          //  mwardno = (TextView) itemView.findViewById(R.id.mtextview_wardno);
            mpost = (TextView) itemView.findViewById(R.id.mpost);
            mcricular = (CircularImageView) itemView.findViewById(R.id.mcricularview);
            mlinear = (LinearLayout)itemView.findViewById(R.id.mlinearlayout_phones);
            mlinear.setOnClickListener(this);
            mcricular.setBorderColor(Color.BLUE);
            mcricular.setBorderWidth(1);
            // Add Shadow with default param
            mcricular.addShadow();
            // or with custom param
            mcricular.setShadowRadius(3);
            mcricular.setShadowColor(Color.RED);
            mphone.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            int id = getAdapterPosition();
            String dial = "tel:" + mlist.get(id).getPhone();
            switch (view.getId()) {
                case R.id.mlinearlayout_phones:
                    Intent callIntent = new Intent(Intent.ACTION_CALL);
                    callIntent.setData(Uri.parse(dial));
                    if (ActivityCompat.checkSelfPermission(mcontext, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                        // TODO: Consider calling
                        //    ActivityCompat#requestPermissions
                        // here to request the missing permissions, and then overriding
                        //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                        //                                          int[] grantResults)
                        // to handle the case where the user grants the permission. See the documentation
                        // for ActivityCompat#requestPermissions for more details.
                        return;
                    }
                    view.getContext().startActivity(callIntent);
                    break;
                case R.id.textview_phone:
                    Intent callIntents = new Intent(Intent.ACTION_CALL);
                    callIntents.setData(Uri.parse(dial));
                    if (ActivityCompat.checkSelfPermission(mcontext, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                        // TODO: Consider calling
                        //    ActivityCompat#requestPermissions
                        // here to request the missing permissions, and then overriding
                        //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                        //                                          int[] grantResults)
                        // to handle the case where the user grants the permission. See the documentation
                        // for ActivityCompat#requestPermissions for more details.
                        return;
                    }
                    view.getContext().startActivity(callIntents);
                break;
            }

        }
    }
}
