package com.pracasinfosys.duhabi.app.Adapter;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Environment;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;


import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.DownloadListener;
import com.androidnetworking.interfaces.DownloadProgressListener;
import com.pracasinfosys.duhabi.app.Activity.Printing;
import com.pracasinfosys.duhabi.app.Activity.Printingactivity;
import com.pracasinfosys.duhabi.app.Downloadclass;
import com.pracasinfosys.duhabi.app.Model.Model_notice_array;
import com.pracasinfosys.duhabi.app.R;
import com.pracasinfosys.duhabi.app.Utils;
import com.jacksonandroidnetworking.JacksonParserFactory;

import java.util.List;

/**
 * Created by Razu on 1/6/2018.
 */

public class Recyclerview_socialsecurity extends RecyclerView.Adapter<Recyclerview_socialsecurity.MyViewHolder> {
    List<Model_notice_array> mlistarray;
    private Context mcontext;
    Downloadclass mclass;
    Printingactivity mprintingclass;
    String activity_url;
    ProgressDialog mdialogue;
    int i;

    public Recyclerview_socialsecurity() {
    }

    public Recyclerview_socialsecurity(List<Model_notice_array> mlistarray, Context mcontext,String activity_url) {
        this.mlistarray = mlistarray;
        this.mcontext = mcontext;
        this.activity_url = activity_url;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View mview = LayoutInflater.from(parent.getContext()).inflate(R.layout.cardview_samajilsurakshya,parent,false);
        return new MyViewHolder(mview);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        Model_notice_array marray = mlistarray.get(position);
        holder.title.setText(marray.getTitle());
        holder.wards.setText(marray.getWard_no());
        i = marray.getType();
    }

    @Override
    public int getItemCount() {
        return mlistarray.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        TextView title,wards;
        TextView mtextview,mtextviewdownload,mtextviewview;
        ImageView mdownload,mviews,mimage_print;

        public MyViewHolder(View itemView) {
            super(itemView);
            title = (TextView)itemView.findViewById(R.id.textview_1_newstitle_sarbajanik);
            wards = (TextView)itemView.findViewById(R.id.textview_wardno_text);
            //title.setOnClickListener(this);
            //itemView.setOnClickListener(this);
            mimage_print=(ImageView)itemView.findViewById(R.id.imageview_print);
            //printing
            mimage_print.setOnClickListener(this);
            mtextviewdownload =(TextView)itemView.findViewById(R.id.textview_downloads);
            mtextviewview =(TextView)itemView.findViewById(R.id.textview_downloadss);
            mdownload=(ImageView)itemView.findViewById(R.id.view_download);
            mviews =(ImageView)itemView.findViewById(R.id.view_views);
            mtextviewview.setOnClickListener(this);
            mdownload.setOnClickListener(this);
            mviews.setOnClickListener(this);
            mtextviewdownload.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            int id = getAdapterPosition();
            mdialogue = new ProgressDialog(view.getContext());
            AndroidNetworking.initialize(mcontext);
            AndroidNetworking.setParserFactory(new JacksonParserFactory());
            switch (view.getId()) {
                case R.id.imageview_print:
                    if(activity_url.equals("socialsecurity")) {


                        // progressdialogue("Printing.Please Wait....");
                        //if(acttivity_url.equals("taxcost")) {
                        // Toast.makeText(mcontext, "Printing.Please Wait...", Toast.LENGTH_SHORT).show();
                        //mprintingclass = new Printingactivity(mcontext, mlistarray.get(id).getFile(),"taxcost");
                        String namess = "samajiksurakshya_" + id + ".pdf";
                        Intent mintess = new Intent(view.getContext(), Printing.class);
                        mintess.putExtra("files", mlistarray.get(id).getFile());
                        mintess.putExtra("filename", namess);
                        view.getContext().startActivity(mintess);
                        }else if(activity_url.equals("health")){
                        String namess = "swasthya_" + id + ".pdf";
                        Intent mintess = new Intent(view.getContext(), Printing.class);
                        mintess.putExtra("files", mlistarray.get(id).getFile());
                        mintess.putExtra("filename", namess);
                        view.getContext().startActivity(mintess);

                    }else{
                        String namess = "shikshya_" + id + ".pdf";
                        Intent mintess = new Intent(view.getContext(), Printing.class);
                        mintess.putExtra("files", mlistarray.get(id).getFile());
                        mintess.putExtra("filename", namess);
                        view.getContext().startActivity(mintess);

                    }
                    break;

                //}
                case R.id.textview_downloads:
                    // if(acttivity_url.equals("taxcost")){
                    //Intent mintens = new Intent(Intent.ACTION_VIEW, Uri.parse(mkharid.get(id).getFile()));
                    //view.getContext().startActivity(mintens);
                    //mclass = new Downloadclass(mcontext,mkharid.get(id).getFile(),"taxcost");
                    //mclass = new Downloadclass(mcontext, mlistarray.get(id).getFile(), "socialsecurity");
                    //progressdialogue("Downloading...");
                    /*new MaterialDialog.Builder(this)
                            .title("Downloading")
                        .content("Please Wait..")
                        .progress(true, 0)
                        .progressIndeterminateStyle(true)
                        .show();*/
                    if(activity_url.equals("socialsecurity")) {

                        String downloadurl = mlistarray.get(id).getFile();
                        String downloadFileName = downloadurl.replace(Utils.mainUrl_socialssecurity, "");
                        //progressdialogue("");
                        //Toast.makeText(mcontext, "Downloading..Please Wait...", Toast.LENGTH_LONG).show();
                        progressdialogue("Downlaoding...Please wait.");
                        //00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000......progressdialogue("Downloading...");
                        downloadingtask_files(downloadurl, downloadFileName);

                    }else if(activity_url.equals("health")){
                        String downloadurls = mlistarray.get(id).getFile();
                        String downloadFileNames = downloadurls.replace(Utils.mainUrl_health, "");
                        //Toast.makeText(mcontext, "Downloading..Please Wait...", Toast.LENGTH_LONG).show();
                        //progressdialogue("Downloading...");
                        progressdialogue("Downlaoding...Please wait.");
                        downloadingtask_files(downloadurls,downloadFileNames);

                    }else{
                        String downloadurls = mlistarray.get(id).getFile();
                        String downloadFileNames = downloadurls.replace(Utils.mainUrl_education, "");
                        //Toast.makeText(mcontext, "Downloading..Please Wait...", Toast.LENGTH_LONG).show();
                        //progressdialogue("Downloading...");
                        progressdialogue("Downlaoding...Please wait.");
                        downloadingtask_files(downloadurls,downloadFileNames);

                    }
                    break;
                case R.id.textview_downloadss:
                    if(activity_url.equals("socialsecurity")){
//                        if(i==0){
//
//                        }else{
                            Intent mintens = new Intent(Intent.ACTION_VIEW, Uri.parse(mlistarray.get(id).getFile()));
                            view.getContext().startActivity(mintens);

                    //Downloadclass mclass = new Downloadclass(mcontext,mkharid.get(i

                    }else if(activity_url.equals("health")){
                        Intent mintens = new Intent(Intent.ACTION_VIEW, Uri.parse(mlistarray.get(id).getFile()));
                        view.getContext().startActivity(mintens);

                    }else{
                        Intent mintens = new Intent(Intent.ACTION_VIEW, Uri.parse(mlistarray.get(id).getFile()));
                        view.getContext().startActivity(mintens);

                    }
                    break;
                case R.id.view_download:
                    //if(acttivity_url.equals("taxcost")){
                        //Intent mintens = new Intent(Intent.ACTION_VIEW, Uri.parse(mkharid.get(id).getFile()));
                        //view.getContext().startActivity(mintens);
                    if(activity_url.equals("socialsecurity")){

                    String downloadurls = mlistarray.get(id).getFile();
                    String downloadFileNames = downloadurls.replace(Utils.mainUrl_socialssecurity, "");
                    //Toast.makeText(mcontext, "Downloading..Please Wait...", Toast.LENGTH_LONG).show();
                    //progressdialogue("Downloading...");
                    progressdialogue("Downlaoding...Please wait.");
                    downloadingtask_files(downloadurls,downloadFileNames);
                    }else if(activity_url.equals("health")){
                        String downloadurls = mlistarray.get(id).getFile();
                        String downloadFileNames = downloadurls.replace(Utils.mainUrl_health, "");
                        //Toast.makeText(mcontext, "Downloading..Please Wait...", Toast.LENGTH_LONG).show();
                        //progressdialogue("Downloading...");
                        progressdialogue("Downlaoding...Please wait.");
                        downloadingtask_files(downloadurls,downloadFileNames);

                    }else{
                        String downloadurls = mlistarray.get(id).getFile();
                        String downloadFileNames = downloadurls.replace(Utils.mainUrl_education, "");
                        //Toast.makeText(mcontext, "Downloading..Please Wait...", Toast.LENGTH_LONG).show();
                        //progressdialogue("Downloading...");
                        progressdialogue("Downlaoding...Please wait.");
                        downloadingtask_files(downloadurls,downloadFileNames);

                    }
                    break;

                case R.id.view_views:
                    //if(acttivity_url.equals("taxcost")){
                    if(activity_url.equals("socialsecurity")) {
                        Intent mintenss = new Intent(Intent.ACTION_VIEW, Uri.parse(mlistarray.get(id).getFile()));
                        view.getContext().startActivity(mintenss);
                        //Downloadclass mclass = new Downloadclass(mcontext,mkharid.get(i

                    }else if(activity_url.equals("health")){
                        Intent mintenss = new Intent(Intent.ACTION_VIEW, Uri.parse(mlistarray.get(id).getFile()));
                        view.getContext().startActivity(mintenss);

                    }else{
                        Intent mintenss = new Intent(Intent.ACTION_VIEW, Uri.parse(mlistarray.get(id).getFile()));
                        view.getContext().startActivity(mintenss);

                    }
                    break;

            }
        }


    }
    public void downloadingtask_files(String fileurl,String flenamses){

        AndroidNetworking.download(fileurl, Environment.getExternalStorageDirectory()+"/"+ Utils.downloadDirectory,flenamses)
                .setTag("downloadTest")
                .setPriority(Priority.MEDIUM)
                .build()
                .setDownloadProgressListener(new DownloadProgressListener() {
                    @Override
                    public void onProgress(long bytesDownloaded, long totalBytes) {
                        // do anything with progress
                        progressdialogue("Downlaoding...Please wait.");
                        //new Handler().postDelayed(new Runnable() {
                        //@Override
                        //public void run() {

                        //Toast.makeText(mcontext, "Downloading..Please Wait..", Toast.LENGTH_SHORT).show();
                        // }
                        //},5000);

                    }
                })
                .startDownload(new DownloadListener() {
                    @Override
                    public void onDownloadComplete() {
                        // do anything after completion
                       //
                        mdialogue.dismiss();
                        Toast.makeText(mcontext, "Download Finished", Toast.LENGTH_SHORT).show();
                        //PrintManager printManager = (PrintManager)mcontext.getSystemService(Context.PRINT_SERVICE);
                        //String jobName =mcontext.getString(R.string.app_name) +
                        //" Document";
                        // printManager.print(jobName, new MyPrintDocumentAdapter(Printing.this,getfilename),null);
                        // finish();
                    }
                    @Override
                    public void onError(ANError error) {
                        // handle error

                        mdialogue.dismiss();
                        Toast.makeText(mcontext, "Failed To Download..Please Try Again Later!!", Toast.LENGTH_SHORT).show();

                        Log.d("ll", "onError: errorddddddddddddddddd"+error.toString());
                    }
                });

    }
    public void progressdialogue(String messages){

        //mdialogue = new ProgressDialog(this);
        mdialogue.setMessage(messages);
        mdialogue.setIndeterminate(false);
        mdialogue.setCancelable(false);
        mdialogue.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        mdialogue.setProgress(10);
        mdialogue.show();
       //  Handler mahand = new Handler();
         //mahand.postDelayed(new Runnable() {
           // @Override
          //public void run() {
          //mdialogue.dismiss();
         //}
          //},4000);
    }
}
