package com.pracasinfosys.duhabi.app.Adapter;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Environment;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.DownloadListener;
import com.androidnetworking.interfaces.DownloadProgressListener;
import com.pracasinfosys.duhabi.app.Activity.Sarbajanik_2_details;
import com.pracasinfosys.duhabi.app.Model.Model_notice_array;
import com.pracasinfosys.duhabi.app.R;
import com.pracasinfosys.duhabi.app.Utils;

import java.util.List;

/**
 * Created by Paru on 2/22/2018.
 */

public class UsersAdapter extends BaseAdapter {

    private Context mContext;
    List<Model_notice_array> marray;
    ProgressDialog  mdialogue;



    public UsersAdapter(Context mContext, List<Model_notice_array> marray) {
        this.mContext = mContext;
        this.marray = marray;

    }
    //    private final String[] web;
//    private final int[] Imageid;
//
//    public UsersAdapter(Context c, String[] web, int[] Imageid ) {
//        mContext = c;
//        this.Imageid = Imageid ;
//        this.web = web;
//
//    }

    @Override
    public int getCount() {

        return marray.size();

    }


    @Override
    public Object getItem(int position) {

        return null;
    }

    @Override
    public long getItemId(int position) {

        return 0;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        View grid;

        if (convertView == null) {

            LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            grid = inflater.inflate(R.layout.activity_listview, null);


        }

        else {
            grid = convertView;

        }

        TextView textView = (TextView) grid.findViewById(R.id.mtextviews);
        Button button = (Button) grid.findViewById(R.id.btn_viewpdf);
        Button button1 = (Button) grid.findViewById(R.id.downloadpdfbtn);
//        ImageView_zoom imageView = (ImageView_zoom)grid.findViewById(R.id.grid_image);
        textView.setText(marray.get(position).getTitle());
        if(marray.get(position).getType()==0){
            button.setVisibility(View.GONE);
        }else{
            button.setVisibility(View.VISIBLE);
            button1.setVisibility(View.VISIBLE);
        }
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Toast.makeText(mContext, "btn", Toast.LENGTH_SHORT).show();
                Intent minten = new Intent(Intent.ACTION_VIEW, Uri.parse(marray.get(position).getFile()));
                minten.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                v.getContext().startActivity(minten);
            }
        });
        String downloadurl = marray.get(position).getFile();
        String downloadFileName = downloadurl.replace(Utils.mainUrl_Download, "");
        //

        button1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Download(position,v);

            }
        });
//        imageView.setImageResource(Imageid[position]);


        return grid;
    }


    public void Download(int position,final View v){

        AndroidNetworking.download(marray.get(position).getFile(), Environment.getExternalStorageDirectory()+"/"+ Utils.downloadDirectory,"download_"+position+".pdf")
                .setTag("downloadTest")
                .setPriority(Priority.MEDIUM)
                .build()
                .setDownloadProgressListener(new DownloadProgressListener() {
                    @Override
                    public void onProgress(long bytesDownloaded, long totalBytes) {
                        // do anything with progress
                        //new Handler().postDelayed(new Runnable() {
                        //@Ov//erride
                        // public void run() {
//                    mdialogue=new ProgressDialog(v.getContext());
//                    mdialogue.setMessage("Downloading Please wait....");
//                    mdialogue.setIndeterminate(false);
//                    mdialogue.setCancelable(false);
//                    mdialogue.setProgressStyle(ProgressDialog.STYLE_SPINNER);
//                    mdialogue.setProgress(10);
//                    mdialogue.show();
                        Toast.makeText(mContext, "Starting Download..Please wait..", Toast.LENGTH_LONG).show();
                        //  progressdialogue("Downloading...Please Wait..",v);
                        // Toast.makeText(Sarbajanik_2_details.this, "Printing..Please Wait..", Toast.LENGTH_SHORT).show();
                        //  }
                        //   },5000);

                    }
                })
                .startDownload(new DownloadListener() {
                    @Override
                    public void onDownloadComplete() {
                        // do anything after completion
                        //mdialogue.dismiss();
                        Toast.makeText(mContext, "Download Finished", Toast.LENGTH_SHORT).show();
                        //PrintManager printManager = (PrintManager)getSystemService(Context.PRINT_SERVICE);
                        //String jobName =getString(R.string.app_name) +
                        // " Document";
                        //printManager.print(jobName, new MyPrintDocumentAdapter(Sarbajanik_2_details.this,getfilename),null);
                        //finish();

                    }
                    @Override
                    public void onError(ANError error) {
                        // handle error
                        // mdialogue.dismiss();
                        Toast.makeText(mContext, "Failed To Download..Please Try Again Later!!", Toast.LENGTH_SHORT).show();
                        Log.d("ll", "onError: errorddddddddddddddddd"+error.toString());
                    }
                });

    }
//
//    public void progressdialogue(String messages,final View v){
//
//        mdialogue = new ProgressDialog(v.getContext());
//        mdialogue.setMessage(messages);
//        mdialogue.setIndeterminate(false);
//        mdialogue.setCancelable(false);
//        mdialogue.setProgressStyle(ProgressDialog.STYLE_SPINNER);
//        mdialogue.setProgress(10);
//        mdialogue.show();
////        Handler mahand = new Handler();
////        mahand.postDelayed(new Runnable() {
////            @Override
////            public void run() {
////                mdialogue.dismiss();
////            }
////        },3000);
//    }
//
//







}
