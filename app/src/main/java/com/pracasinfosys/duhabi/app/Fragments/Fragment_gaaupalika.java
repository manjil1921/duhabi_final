package com.pracasinfosys.duhabi.app.Fragments;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.pracasinfosys.duhabi.app.Adapter.Recyclerview_padhadhikarikobibaran;
import com.pracasinfosys.duhabi.app.Interface_retrofit;
import com.pracasinfosys.duhabi.app.Model.Model_notice_array;
import com.pracasinfosys.duhabi.app.Model.Model_notice_main;
import com.pracasinfosys.duhabi.app.R;
import com.pracasinfosys.duhabi.app.Restapi.Apiclient;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Razu on 1/6/2018.
 */

public class Fragment_gaaupalika extends Fragment {
    Interface_retrofit minterface;
    RecyclerView mrecycler;
    RecyclerView.LayoutManager mlayoutmanager;
    //Recyclerview_Suchanasamachar madapter;
    SwipeRefreshLayout mswiperefresh;
    List<Model_notice_array> mlist_sarbajanik = new ArrayList<>();




    public Fragment_gaaupalika() {
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_gaaupalika, container, false);

        mrecycler=(RecyclerView)view.findViewById(R.id.mrecyclerview);
        mswiperefresh =(SwipeRefreshLayout)view.findViewById(R.id.mswiprefresh);

        mswiperefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                mswiperefresh.setRefreshing(true);
                if(isconnectd()){
                    getreestapi_sarbajanikkharid();
                    Toast.makeText(getContext(),"Refresh Successfull",Toast.LENGTH_SHORT).show();
                    mswiperefresh.setRefreshing(false);
                }else{
                    //mlinear.setVisibility(View.VISIBLE);
                    Toast.makeText(getContext(),"Refresh Successfull",Toast.LENGTH_SHORT).show();
                    mswiperefresh.setRefreshing(false);
                }

            }
        });
        mlayoutmanager = new LinearLayoutManager(getContext(),LinearLayoutManager.VERTICAL,false);
        mrecycler.setLayoutManager(mlayoutmanager);
        getreestapi_sarbajanikkharid();





        return view;
    }
    public boolean isconnectd(){
        ConnectivityManager mconn = (ConnectivityManager)getContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo minfo =mconn.getActiveNetworkInfo();
        if(minfo != null && minfo.isConnected()){
            return true;
        }
        return false;
    }
    public void getreestapi_sarbajanikkharid(){
        minterface = Apiclient.getApiclient().create(Interface_retrofit.class);
        Call<Model_notice_main> msarbajanik = minterface.mgetbibaran("2");
        msarbajanik.enqueue(new Callback<Model_notice_main>() {
            @Override
            public void onResponse(Call<Model_notice_main> call, Response<Model_notice_main> response) {
                mlist_sarbajanik = response.body().getResult();
                Log.d("suc", "onResponse: kkkkkkkkkkk"+mlist_sarbajanik);
                mrecycler.setAdapter(new Recyclerview_padhadhikarikobibaran(mlist_sarbajanik,getActivity()));

            }

            @Override
            public void onFailure(Call<Model_notice_main> call, Throwable t) {
                Log.d("failed", "onResponse: kkkkkkkkkkk"+t.toString());
            }
        });
    }
}
