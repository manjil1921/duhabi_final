package com.pracasinfosys.duhabi.app.Fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.pracasinfosys.duhabi.app.R;

/**
 * Created by Razu on 1/6/2018.
 */

public class Fragment_odawise extends Fragment {
    public Fragment_odawise() {
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_odawise, container, false);
        return view;
    }
}
