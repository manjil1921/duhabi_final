package com.pracasinfosys.duhabi.app;

import com.pracasinfosys.duhabi.app.Model.Model_notice_main;
import com.pracasinfosys.duhabi.app.Model.Model_post_data;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

/**
 * Created by user on 1/4/2018.
 */

public interface Interface_retrofit {
    @GET("getWorker?profession_id")
    Call<Model_notice_main> mgetworkders(@Query("profession_id") String profession_id);
    @GET("getNews")
    Call<Model_notice_main> mgetnews();
    //@GET("getNotice?sem_id=3")
    //Call<Model_notice_main> mgetnotices();
    @GET("getPublicPurchase")
    Call<Model_notice_main> mgetsarbajanik();
    @GET("getTaxCost")
    Call<Model_notice_main> mgettaxcost();
    @GET("getApplicationFormat")
    Call<Model_notice_main> mgetnibedankodhacha();
    @GET("getCitizenCharter")
    Call<Model_notice_main> mgetbadapatra();
    @GET("getSocialSecurity")
    Call<Model_notice_main> mgetsocialsecurity(@Query("ward_id") String ward_id);
    @GET("getStaffDetails")
    Call<Model_notice_main> mgetbibaran(@Query("ward_id") String ward_id);
    @GET("getContactDetails")
    Call<Model_notice_main> mgetinfo(@Query("ward_id") String ward_id);
    @GET("getSocialSecurityDetail")
    Call<Model_notice_main> mgetsecutity_details();
    @POST("complaint")
    @FormUrlEncoded
    Call<Model_post_data> mpostfeedback(@Field("name") String name, @Field("phone") String phone, @Field("address") String address, @Field("sub") String sub, @Field("body") String body);
    @GET("getOffice")
    Call<Model_notice_main> mgetimportantno();
    @GET("getVitalRegistration")
    Call<Model_notice_main> mgetyaktibibarans(@Query("ward_id") String ward_id);
    @GET("getConsumer")
    Call<Model_notice_main> mgetconsumer();
    @GET("getPersonalSecurity")
    Call<Model_notice_main> mgetpersonalsecurity();
    @GET("getEmail")
    Call<Model_notice_main> mgetEmail();
    @GET("getNumbers")
    Call<Model_notice_main> mgetdetailss(@Query("office_id") String office_id);
    //getshikshya
    @GET("getHealth")
    Call<Model_notice_main> mgethelath();
    //gethealth
    @GET("getEducation")
    Call<Model_notice_main> mgetshikshya();

    @GET("getIntro")
    Call<Model_notice_main> mgetintro(@Query("ward_id") String ward_id);

    @GET("getWards")
    Call<Model_notice_main> mgetward();
    //for samjik,surakshya,shikshya
    @GET("getWardSocial")
    Call<Model_notice_main> mgetward_samajik();
    @GET("getDownload")
    Call<Model_notice_main> mdownloads();



}
