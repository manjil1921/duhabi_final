package com.pracasinfosys.duhabi.app.Model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by user on 1/4/2018.
 */

public class Model_details_samachar {
    @SerializedName("id")
    private int id;
    @SerializedName("title")
    private String title;
    @SerializedName("content")
    private String content;
    @SerializedName("file")
    private String file;
    @SerializedName("date")
    private int date;

    public Model_details_samachar(int id, String title, String content, String file, int date) {
        this.id = id;
        this.title = title;
        this.content = content;
        this.file = file;
        this.date = date;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getFile() {
        return file;
    }

    public void setFile(String file) {
        this.file = file;
    }

    public int getDate() {
        return date;
    }

    public void setDate(int date) {
        this.date = date;
    }
}