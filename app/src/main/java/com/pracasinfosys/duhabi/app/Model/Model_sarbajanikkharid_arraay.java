package com.pracasinfosys.duhabi.app.Model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by user on 1/5/2018.
 */

public class Model_sarbajanikkharid_arraay {
    @SerializedName("id")
    private String id;
    @SerializedName("title")
    private String title;
    @SerializedName("content")
    private String content;
    @SerializedName("date")
    private String date;
    @SerializedName("file")
    private String file;

    public Model_sarbajanikkharid_arraay(String id, String title, String content, String date, String file) {
        this.id = id;
        this.title = title;
        this.content = content;
        this.date = date;
        this.file = file;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getFile() {
        return file;
    }

    public void setFile(String file) {
        this.file = file;
    }
}
