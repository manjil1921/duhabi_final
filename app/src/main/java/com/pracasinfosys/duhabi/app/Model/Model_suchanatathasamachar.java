package com.pracasinfosys.duhabi.app.Model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by user on 1/4/2018.
 */

public class Model_suchanatathasamachar {


    @SerializedName("ack")
    private String ack;
    @SerializedName("ack_msg")
    private String ack_msg;
    @SerializedName("result")
    private List<Model_details_samachar> result;

    public Model_suchanatathasamachar(String ack, String ack_msg, List<Model_details_samachar> result) {
        this.ack = ack;
        this.ack_msg = ack_msg;
        this.result = result;
    }

    public String getAck() {
        return ack;
    }

    public void setAck(String ack) {
        this.ack = ack;
    }

    public String getAck_msg() {
        return ack_msg;
    }

    public void setAck_msg(String ack_msg) {
        this.ack_msg = ack_msg;
    }

    public List<Model_details_samachar> getResult() {
        return result;
    }

    public void setResult(List<Model_details_samachar> result) {
        this.result = result;
    }
}
