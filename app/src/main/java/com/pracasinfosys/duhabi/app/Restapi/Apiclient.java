package com.pracasinfosys.duhabi.app.Restapi;


import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by user on 1/4/2018.
 */

public class Apiclient {
    //public static final String BASE_URL = "http://demo.kulchan.com/egov/api/";
    public static final String BASE_URL = "https://pracasinfosys.com/duhabi/api/";
    public static Retrofit retrofit = null;
    public static Retrofit getApiclient(){
        if(retrofit == null){
            retrofit = new Retrofit.Builder().baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create()).build();

        }
        return retrofit;

    }

}
