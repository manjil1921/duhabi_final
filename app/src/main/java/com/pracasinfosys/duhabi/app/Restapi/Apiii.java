package com.pracasinfosys.duhabi.app.Restapi;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Dell on 2/8/2018.
 */

public class Apiii {
    public static final String BASE_URL = "http://demo.kulchan.com/sriyog/api/";
    public static Retrofit retrofit = null;
    public static Retrofit getApiclient(){
        if(retrofit == null){
            retrofit = new Retrofit.Builder().baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create()).build();

        }
        return retrofit;

    }
}
