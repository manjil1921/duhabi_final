package com.pracasinfosys.duhabi.app;

/**
 * Created by Razu on 12/5/2017.
 */

public class Utils {

    public static final String downloadDirectory = "Duhabi File Downloads ";
    public static final String mainUrl = "http://pracasinfosys.com/duhabi/uploads/";
    public static final String mainUrl_applicationformat = "http://pracasinfosys.com/duhabi/uploads/application_format";
    //public static final String mainUrl_staffdetails = "http://demo.kulchan.com/egov/uploads/staff_details";
    public static final String mainUrl_taxcost = "http://pracasinfosys.com/duhabi/uploads/tax_cost";
    public static final String mainUrl_publicpurchase = "http://pracasinfosys.com/duhabi/uploads/public_purchase";
    // public static final String downloadPdfUrl = "http://androhub.com/demo/demo.pdf";
    //public static final String downloadPdfUrl = "http://demo.acepirit.com/bsccsit/uploads/syllabus/0IWlu783.pdf";
    public static final String mainUrl_socialssecurity = "http://pracasinfosys.com/duhabi/uploads/social_security";
    public static final String mainUrl_vitalregistration = "http://pracasinfosys.com/duhabi/uploads/vital_registration";
    public static final String mainUrl_personalsecurity = "http://pracasinfosys.com/duhabi/uploads/personal_security";
    public static final String mainUrl_consumer = "http://pracasinfosys.com/duhabi/uploads/consumer";
    public static final String mainUrl_health = "http://pracasinfosys.com/duhabi/uploads/health";
    public static final String mainUrl_education = "http://pracasinfosys.com/duhabi/uploads/education";
    public static final String mainUrl_Download = "http://pracasinfosys.com/duhabi/uploads/download";


}
